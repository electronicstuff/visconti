$(function () {

    window.visconti.slider();
    window.visconti.visibility();
    window.visconti.popin();
    window.visconti.menu();
    window.visconti.background();
    window.visconti.clicks();
    window.visconti.search();
    window.visconti.message();

});
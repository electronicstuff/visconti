<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'visconti');

/** MySQL database username */
define('DB_USER', 'visconti');

/** MySQL database password */
define('DB_PASSWORD', '7a84d7216558acb840d5e83d4b0e470f6d1af4c5ffed9860a9b0f70dee4b09f9');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1O!iig;Q~%e-T<4I#z3;aa%[$aJHJhEy~l>kKY,5j!JrX;E^M9*42e!~cdexiTZU');
define('SECURE_AUTH_KEY',  'WMNW:Ad@$I&*`DIsyXzb[q2%IDC97D(V+_]Q1#`KeMQ$@+J~-e1I^EG=XcjZnu0Z');
define('LOGGED_IN_KEY',    '+[`55ETzmXsW7@&X+T7enVK+Z1b~Hj<YB?{6KQ.@>-MB0^?1YN4-:XjOl#x//%up');
define('NONCE_KEY',        '/x_;DNQ9<F1yBh%HSY2+Y=j5M~HRXT@k%STsaJ#N?;cb@O&~OsTVGrsP2NTfqy-}');
define('AUTH_SALT',        'kc/yU.V)Y_iR~{HN:#0/G2Mpyq=p _GFA-lXw}Z~E!qF<VW 6%7es&f1qAy92;vA');
define('SECURE_AUTH_SALT', '~;}1]K0P~8hjvTF 3sVT:3B&CDy=m-]%>qkM$jrg@yx ]@C{|Ho$@OEK#Y%_Z7Q3');
define('LOGGED_IN_SALT',   'I5(9%//+yBa/;JGDB<=)mb/~4v.v:@`mpzXj NE._5wOL3}nZ+!0c?lBLt6v$`E@');
define('NONCE_SALT',       ' moTg8A%7m`IfU>|nfPsTj{r?YI6c(O$46oJ[b`(mRU8Ybk6J,Z/0S5uI39Q[)J*');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

<?php get_header(); ?>
<?php
$id         = get_the_ID();
$cat_url    = get_post_type_archive_link( 'coach' );
$function   = get_field( 'coach_role' );
$image      = get_field( 'coach_image' );
$image      = wp_get_attachment_image_src( $image, 'coach-person' );
$image      = $image[0];
$twitter    = get_field( 'coach_twitter' );
$linkedin   = get_field( 'coach_linkedin' );
$experience = get_field( 'coach_experience' );
$quote      = get_field( 'coach_quote' );
$videoUrl   = get_field( 'coach_video' );
$videoID    = substr( $videoUrl, strrpos( $videoUrl, '?v=' ) + 3 );
$video      = $videoID ? 'https://www.youtube.com/embed/' . $videoID : '';
$videoTitle = get_field( 'coach_video_title' );
$videoText  = get_field( 'coach_video_text' );
$text       = get_field( 'coach_text' );
$email      = get_field('coach_email');
?>
<?php
if(isset($_POST['email-input']) && $email):
    $to      = $email;
    $email   = $_POST['email-input'];
	$message = $_POST['message-input'];
    $subject = $_POST['object-input'];
    $headers = "From:".$email;

	if (filter_var($email, FILTER_VALIDATE_EMAIL) && filter_var($message, FILTER_SANITIZE_STRING)) {
		mail($to,$subject,$message,$headers, "-f ".$email);
	}
endif;
?>

	<section class="container">
		<div class="section coach-pres">
			<p>
				<a href="<?php echo $cat_url; ?>" class="link-simple accent-text">
					<i class="icon-arrow-left"></i>
					<strong><?php echo __( 'Retour à la liste des coachs', 'vlang' ); ?></strong>
				</a>
			</p>
			<div class="coach-content">
				<div class="presentation">
					<div class="synopsis">
						<img class="picture fluid" src="<?php echo $image; ?>" alt="Picture of coach">
						<div class="infos">
							<h1 class="name hyphenate">
								<?php echo get_the_title(); ?>
							</h1>
							<h2 class="occupation">
								<?php if ( $function ): ?>
									<?php echo $function; ?>
								<?php endif; ?>
							</h2>
							<div class="links">
								<?php if ( $twitter ): ?>
									<a href="<?php echo $twitter; ?>" class="link-simple round icon-twitter"></a>
								<?php endif; ?>
								<?php if ( $linkedin ): ?>
									<a href="<?php echo $linkedin; ?>"
									   class="link-simple round icon-linkedin-square"></a>
								<?php endif; ?>
							</div>
                            <?php if($email): ?>
							<a href="#" class="link-simple rounded accent icon consult">
								<?php echo __( 'Contacter le coach', 'vlang' ); ?>
								<i class="icon-arrow-right"></i>
							</a>
                            <?php endif; ?>
						</div>
					</div>
					<div class="details">
						<div class="experience">
							<?php if ( $experience ): ?>
								<h6 class="title">
									<strong><?php echo __( 'Expérience', 'vlang' ); ?></strong>
								</h6>
								<div class="content">
									<?php echo $experience; ?>
								</div>
							<?php endif; ?>
							<h6 class="title">
								<strong><?php echo __( 'Langues', 'vlang' ); ?></strong>
							</h6>
                            <?php
        					if( have_rows('coach_flags') ):
                                ?>
                                <div class="flags">
                                <?php
        						while ( have_rows('coach_flags') ) : the_row();
        							$flag = get_sub_field('coach_flag');
        							$flag = wp_get_attachment_image_src( $flag, 'flag');
        							$flag = $flag[0];
        						?>
        						    <img src="<?php echo $flag; ?>" />
        						<?php
        						endwhile;
                                ?>
                                </div>
                                <?php
        					endif;
        					?>
						</div>
						<div class="longform">
							<?php if ( $quote ): ?>
								<div class="deco-line quote-line">
									<?php echo $quote; ?>
								</div>
							<?php endif; ?>
							<?php if ( $video ): ?>
								<div class="deco-line video-line">
									<div class="embed-container">
										<iframe src="<?php echo $video; ?>" frameborder="0"
										        allowfullscreen></iframe>
									</div>
									<?php if ( $videoTitle ): ?>
										<h3 class="title">
											<?php echo $videoTitle; ?>
										</h3>
									<?php endif; ?>
									<?php if ( $videoText ): ?>
										<p>
											<?php echo $videoText; ?>
										</p>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ( $text ): ?>
								<?php echo $text; ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<?php get_template_part( 'templates/coach', 'track' ); ?>
			</div>
		</div>
		<div class="popin-contact flex-center-full hidden">
			<div class="content">
				<div class="title">
					<p>
						<?php echo __('Contacter', 'vlang'); ?>
					</p>
					<h3 class="name hyphenate">
						<?php echo get_the_title(); ?>
					</h3>
				</div>
				<form action="" method="POST">
                    <input type="hidden" name="coach" value="<?php echo get_the_title(); ?>"/>
                    <div>
						<input name="email-input" type="email" placeholder="<?php echo __('Email', 'vlang'); ?>" required />
					</div>
                    <div>
						<input name="object-input" type="text" placeholder="<?php echo __('Objet', 'vlang'); ?>" required />
					</div>
					<div>
						<textarea name="message-input" rows="5" required placeholder="<?php echo __('Message', 'vlang'); ?>"></textarea>
					</div>

					<button type="submit" class="link-simple rounded accent hide-tablet">
						<?php echo __('Envoyer', 'vlang'); ?>
						<i class="icon-arrow-right"></i>
					</button>
				</form>
				<a href="#" class="icon-cross link-simple no-hover close"></a>
			</div>
		</div>

		<?php get_template_part( 'templates/home', 'insights' ); ?>
		<?php get_template_part( 'templates/general', 'twitter' ); ?>
	</section>
<?php
get_footer();

<?php
/**
 * Template Name: Homepage
 *
 * @package WordPress
 */

get_header(); ?>

<?php get_template_part( 'templates/home', 'slider' ); ?>
<section class="container home-container">
    <?php get_template_part( 'templates/home', 'map' ); ?>
    <?php get_template_part( 'templates/home', 'offres' ); ?>
    <?php get_template_part( 'templates/home', 'videos' ); ?>
    <?php get_template_part( 'templates/home', 'become' ); ?>
    <?php get_template_part( 'templates/home', 'coaching' ); ?>
    <?php get_template_part( 'templates/home', 'insights' ); ?>
    <?php get_template_part( 'templates/general', 'twitter' ); ?>
</section>

<?php get_footer(); ?>

<?php
$intro_text = get_field('coach_intro', 'options');
?>
<div class="coachs-intro">
    <h1 class="title zone-title">
        <?php echo __('Nos coachs', 'vlang'); ?>
    </h1>

    <h2 class="subtitle">
        <?php if($intro_text): ?>
            <?php echo $intro_text; ?>
        <?php endif; ?>
    </h2>

    <p class="icon">
        <i class="icon-sphere"></i>
    </p>
</div>

<?php
$title = get_field('blue_title', 'options');
?>
<?php if($_GET["lang"] != "en"): ?>
<div class="blue-books">
    <div class="container">
        <form action="" method="post" enctype="multipart/form-data">
            <input type="hidden" name="contact_form" value="newsletter"/>
            <h4 class="title">
                <strong>
                    <?php echo $title; ?>
                </strong>
            </h4>
            <p class="content">
                <i class="icon-envelope-o"></i>
                <input type="email" required name="email" placeholder="<?php echo __("Adresse mail", "vlang"); ?>" class="raw no-focus" required />
            </p>
            <p class="action">
                <button type="submit" class="link-simple rounded accent full-width">
                    <?php echo __("S'inscrire", "vlang"); ?>
                </button>
            </p>
        </form>
    </div>
</div>
<?php echo do_shortcode( '[cfdb-save-form-post]' ); ?>
<?php endif; ?>

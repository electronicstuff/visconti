=== Post Date Time Change ===
Contributors: Katsushi Kawamori
Donate link: http://pledgie.com/campaigns/28307
Tags: admin, attachment, attachments, date, edit, media, page, pages, post, posts, time
Requires at least: 3.6.0
Tested up to: 4.6
Stable tag: 3.02
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Collectively change the date and time of each article of post or page or media library.

== Description ==

* Collectively change the date and time of each article of post or page or media library.
* Work with [DateTimePicker](http://xdsoft.net/jqplugins/datetimepicker/). jQuery plugin select date/time.

== Installation ==

1. Upload `post-date-time-change` directory to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

none

== Screenshots ==

1. Posts
2. Media

== Changelog ==

= 3.02 =
Fixed problem of read posts overlapping.
Change display.
Add items display.
Can save the settings for each user.
Fixed problem of uninstall.

= 3.01 =
Capability change to "edit_posts" from "manage_options".
Add users display.

= 3.00 =
Add text search.
Fixed problem of database read.

= 2.88 =
Add sort.
Fixed problem of pagination.

= 2.87 =
Fixed problem of pagination.

= 2.86 =
Change pagination style.

= 2.85 =
Add types filter.
Move to any page.
Fixed problem of enter key.

= 2.84 =
/languages directory is deleted.

= 2.83 =
Supported GlotPress.

= 2.82 =
Add error handling for Media.

= 2.81 =
Fixed problem of search for mime type.

= 2.8 =
Javascript and CSS will be loaded only to the required page.

= 2.7 =
Add change of draft posts.

= 2.6 =
Fixed display.

= 2.5 =
Fixed the problem of pagenation.

= 2.4 =
Add progress display.
Change /languages.

= 2.3 =
Add the revision.

= 2.2 =
Fixed the problem of the display of DateTimePicker at the time of application of the category filter and MIME type filter.

= 2.1 =
Add mime types filter.
Change of appearance.
Change /languages.

= 2.0 =
Add categories filter.
Supported responsive tab menu.

= 1.4 =
Add screen of donate.
Change the display of the message.
Change readme.txt.
Change /languages.

= 1.3 =
Fixed a problem of Java Script for admin screen.

= 1.2 =
Change management screen to responsive menu design.

= 1.1 =
Attachments organize into month- and year-based folders by automatic.

= 1.0 =

== Upgrade Notice ==

= 3.02 =
= 3.01 =
= 3.00 =
= 2.88 =
= 2.87 =
= 2.86 =
= 2.85 =
= 2.84 =
= 2.83 =
= 2.82 =
= 2.81 =
= 2.8 =
= 2.7 =
= 2.6 =
= 2.5 =
= 2.4 =
= 2.3 =
= 2.2 =
= 2.1 =
= 2.0 =
= 1.4 =
= 1.3 =
= 1.2 =
= 1.1 =
= 1.0 =


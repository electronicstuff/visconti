<div class="track-record">
    <?php if( have_rows('coach_track_record') ): ?>
    <div class="record-wrapper">
        <p class="section-title record">
            <strong>
                <?php echo __('Track record', 'vlang'); ?>
            </strong>
        </p>

        <ul class="list no-style">
            <?php while( have_rows('coach_track_record') ): the_row();
            $title = get_sub_field('record_title');
            $text  = get_sub_field('record_text');
            $image = get_sub_field('record_image');
            $image = wp_get_attachment_image_src( $image, 'coach-track');
            $image = $image[0];
            ?>
            <li class="record">
                <div class="logo">
                    <img src="<?php echo $image; ?>" alt="">
                </div>

                <h4 class="title">
                    <?php echo $title; ?>
                </h4>
                <div class="description">
                    <?php echo $text; ?>
                </div>
            </li>
            <?php endwhile; ?>
        </ul>
    </div>
    <?php endif; ?>
</div>

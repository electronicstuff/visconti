<?php
$title = get_field( 'home_offers_title' );
?>
<div class="section offers generic-push">
	<h2 class="zone-title text-center">
		<?php echo $title; ?>
	</h2>
	<?php if ( have_rows( 'home_offers' ) ): ?>
	<div class="swiper-container offers-slider generic-push-slider js-push-slider">
		<div class="swiper-wrapper offers-wrapper generic-push-wrapper js-push-wrapper">
			<?php while ( have_rows( 'home_offers' ) ): the_row();
				$title = get_sub_field( 'home_offer_title' );
				$text  = get_sub_field( 'home_offer_text' );
				$url   = get_sub_field( 'home_offer_button_url' );
				$image = get_sub_field( 'home_offer_image' );
				$image = wp_get_attachment_image_src( $image, 'homepage-offre' );
				$image = $image[0];
				?>
				<div class="js-push-item offer generic-push-item">
					<div class="title-container">
						<h6 class="title-back">
							<?php echo $title; ?>
						</h6>
					</div>
					<div class="offer-container generic-push-item-container">
						<div class="left">
							<img src="<?php echo $image; ?>" alt="<?php echo $title; ?> thumbnail">
						</div>
						<div class="right">
							<p class="content dot-ellipsis dot-resize-update">
								<?php echo $text; ?>
							</p>
							<a class="button-next link-simple no-hover corner" href="<?php echo $url; ?>">
								<span class="link icon-arrow-right"></span>
							</a>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
		</div>
		<div class="swiper-pagination offers-pagination"></div>
		<?php endif; ?>
	</div>
</div>
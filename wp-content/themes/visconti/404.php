<?php get_header(); ?>

<section class="container generic">
    <div class="generic-container flex-center-v">
        <p class="fourofour">
            <span><?php echo __('404', 'vlang'); ?></span><br>
            <?php echo __('Page non trouvée', 'vlang'); ?>
        </p>
    </div>
</section>


<?php
get_footer();

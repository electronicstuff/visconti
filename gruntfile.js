module.exports = function (grunt) {
    require('load-grunt-config')(grunt, {
        data: {
            theme: 'wp-content/themes/visconti',
            assets: 'wp-content/themes/visconti/assets'
        }
    });
};
<?php if( have_rows('home_pushs') ): ?>
<div class="section pushes">
	<div class="swiper-container push-slider">
		<div class="swiper-wrapper push-wrapper">
        <?php while( have_rows('home_pushs') ): the_row();
            $title      = get_sub_field('home_push_title');
            $article    = (get_sub_field_object('home_push_url'));
            $articleURL = $article['value'];
            $image      = get_sub_field('home_push_image');
            $image      = wp_get_attachment_image_src( $image, 'homepage-pushs');
            $image      = $image[0];
        ?>
            <div class="push swiper-slide">
                <h6 class="title-back">
                    <?php echo $title; ?>
                </h6>
                <img class="fluid" src="<?php echo $image; ?>" alt="">
                <a class="button-next link-simple no-hover corner" href="<?php echo $articleURL; ?>">
                    <span class="link icon-arrow-right"></span>
                </a>
            </div>
        <?php endwhile; ?>
        </div>

        <div class="swiper-pagination push-pagination"></div>
    </div>
</div>
<?php endif; ?>
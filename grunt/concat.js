module.exports = {
    "user": {
        "files": [
            {
                "src": [
                    "<%= assets %>/js/modules/*.js",
                    "<%= assets %>/js/*.js"
                ],
                "dest": "<%= theme %>/js/user.js"
            }
        ]
    },
    "libjs": {
        "files": [
            {
                "expand": true,
                "cwd": "bower_components/",
                "src": [
                    "jquery/dist/jquery.js",
                    "Swiper/dist/js/swiper.jquery.js",
                    "enquire/dist/enquire.min.js",
                    "jQuery.dotdotdot/src/jquery.dotdotdot.min.js"
                ],
                "dest": "<%= theme %>/js/vendor.js",
                "rename": function (dest) {
                    return dest;
                }
            }
        ]
    },
    "libcss": {
        "files": [
            {
                "expand": true,
                "cwd": "bower_components/",
                "src": [
                    "Swiper/dist/css/swiper.min.css"
                ],
                "dest": "<%= theme %>/css/vendor.css",
                "rename": function (dest) {
                    return dest;
                }
            }
        ]
    }
};
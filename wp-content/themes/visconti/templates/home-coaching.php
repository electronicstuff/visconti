<?php if(get_field('home_coaching_title')):
$title      = get_field('home_coaching_title');
$text       = get_field('home_coaching_text');
$videoUrl   = get_field('home_coaching_video');
$videoID    = substr($videoUrl, strrpos($videoUrl, '?v=') + 3);
$video      = 'https://www.youtube.com/embed/'.$videoID;
$article    = (get_field_object('home_coaching_page'));
$articleURL = get_permalink($article['value'][0]);
?>
<div class="section coaching">
    <h2 class="zone-title big">
        <?php echo $title; ?>
    </h2>
    <div class="content">
        <?php echo $text; ?>
        <?php if($article): ?>
        <a href="<?php echo $articleURL; ?>" class="hide-tablet link-simple rounded accent">
            <?php echo __('Voir toutes les vidéos', 'vlang'); ?>
        </a>
        <?php endif; ?>
    </div>
    <div class="media">
        <div class="embed-container">
            <iframe src="<?php echo $video; ?>" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
    <div class="hide-mobile hide-desktop">
        <?php if($article): ?>
        <a href="<?php echo $articleURL; ?>" class="link-simple rounded accent">
            <?php echo __('Voir toutes les vidéos', 'vlang'); ?>
        </a>
        <?php endif; ?>
    </div>
</div>
<?php endif; ?>

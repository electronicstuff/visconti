<?php
$title  = get_field('home_experience_title');
$slider = get_field('home_experience_slider');
?>
<?php if($title): ?>
<?php if( have_rows('home_experience_slider') ): ?>
<div class="section experience">
	<div class="description">
		<h2 class="zone-title inverse hyphenate">
			<?php echo $title; ?>
		</h2>
		<div class="content">
			<div class="buttons xp-slider-button">
				<a data-direction="prev" class="link-simple round text-center icon-arrow-left"></a>
				<a data-direction="next" class="link-simple round text-center icon-arrow-right"></a>
			</div>
			<div class="swiper-container content-slider xp-slider">
				<ul class="swiper-wrapper no-style">
                    <?php while( have_rows('home_experience_slider') ): the_row();
                    $title      = get_sub_field('home_experience_slide_title');
                    $name       = get_sub_field('home_experience_slide_name');
                    $text       = get_sub_field('home_experience_slide_text');
                    $article    = (get_sub_field_object('home_experience_slide_article'));
                    $articleURL = get_permalink($article['value'][0]);
                    ?>
					<li class="swiper-slide content-wrapper">
                        <p class="arrows"></p>
                        <p class="role">
                            <strong>
                                <?php echo $title; ?>
                            </strong>
                        </p>
                        <p class="name">
                            <strong><?php echo $name; ?></strong>
                        </p>
                        <p class="text quotes">
                            <?php echo $text; ?>
                        </p>
                        <p>
                            <a href="<?php echo $articleURL; ?>" class="link-simple rounded accent">
                                <?php echo __('En savoir plus', 'vlang'); ?>
                                <i class="icon-arrow-right"></i>
                            </a>
                        </p>
                    </li>
                    <?php endwhile; ?>
				</ul>
			</div>
		</div>
	</div>
	<div class="video-container">
		<div class="swiper-container video-slider xp-slider">
			<ul class="swiper-wrapper no-style">
                <?php while( have_rows('home_experience_slider') ): the_row();
                $videoUrl = get_sub_field('home_experience_slide_video');
                $videoID  = substr($videoUrl, strrpos($videoUrl, '?v=') + 3);
                $video    = 'https://www.youtube.com/embed/'.$videoID;
                ?>
				<li class="swiper-slide video">
                    <div class="embed-container">
                        <iframe src="<?php echo $video; ?>" frameborder="0" allowfullscreen></iframe>
                    </div>
                </li>
                <?php endwhile; ?>
			</ul>
		</div>
	</div>
</div>
<?php endif; ?>
<?php endif; ?>

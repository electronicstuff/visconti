module.exports = {
  "css" : {
    "options" : {
      "processors" : [
        require('autoprefixer')({
          browsers: ['last 2 versions', 'IE >= 10', 'iOS >= 7']
        })
      ]
    },
    "files" : [{
      "src" : "<%= theme %>/css/main.css",
      "dest" : "<%= theme %>/css/main.css"
    }]
  }
};
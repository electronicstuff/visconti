(function() {

    window.visconti = window.visconti || {};
    window.visconti.message = message;

    function message(){
        var $message = $('.message.user');
        var $close = $message.find('.close');

        if($message.length && $close.length){
            $close.on('click', function(){
                $message.remove();
            });
        }
    }

})();
<?php
$twitterName = get_field('twitter_name', 'options');

if($twitterName):
	// Tweet amount and name
	$tweets = getTweets(4, $twitterName, array('trim_user'=>false));
	?>
	<div class="section twitter">
		<div class="swiper-container twitter-slider">
			<div class="swiper-wrapper">
				<div class="logo icon-twitter swiper-slide slide">
					<p class="account">
						<a class="link" href="https://www.twitter.com/<?php echo $twitterName; ?>">
							<?php echo $twitterName ?>
						</a>
					</p>
				</div>
				<?php
				if(is_array($tweets)):
					foreach($tweets as $tweet):
						$the_tweet = $tweet['text'];
				        if(is_array($tweet['entities']['user_mentions'])){
				            foreach($tweet['entities']['user_mentions'] as $key => $user_mention){
				                $the_tweet = preg_replace(
				                    '/@'.$user_mention['screen_name'].'/i',
				                    '<a href="http://www.twitter.com/'.$user_mention['screen_name'].'" target="_blank">@'.$user_mention['screen_name'].'</a>',
				                    $the_tweet);
				            }
				        }
				        if(is_array($tweet['entities']['hashtags'])){
				            foreach($tweet['entities']['hashtags'] as $key => $hashtag){
				                $the_tweet = preg_replace(
				                    '/#'.$hashtag['text'].'/i',
				                    '<a href="https://twitter.com/search?q=%23'.$hashtag['text'].'&src=hash" target="_blank">#'.$hashtag['text'].'</a>',
				                    $the_tweet);
				            }
				        }
				        if(is_array($tweet['entities']['urls'])){
				            foreach($tweet['entities']['urls'] as $key => $link){
				                $the_tweet = preg_replace(
				                    '`'.$link['url'].'`',
				                    '<a href="'.$link['url'].'" target="_blank">'.$link['url'].'</a>',
				                    $the_tweet);
				            }
				        }

						$userImage  = $tweet['user']['profile_image_url'];
						$userName   = $tweet['user']['name'];
						$screenName = $tweet['user']['screen_name'];
						?>
						<div class="tweet swiper-slide slide">
			                <div class="top">
			                    <div class="author">
			                        <img class="picture" src="<?php echo $userImage; ?>" alt="author">
			                        <p class="info">
			                            <strong class="name">
			                                <?php echo $userName; ?>
			                            </strong><br />
			                            <a href="#" class="link-simple">
			                                @<?php echo $screenName ?>
			                            </a>
			                        </p>
			                    </div>
			                    <a href="#" class="icon-twitter link-simple no-hover"></a>
			                </div>
			                <p class="content">
			                    <?php echo $the_tweet; ?>
			                </p>

			                <div class="bottom">
			                    <div class="intents">
			                        <a class="icon-heart-o link-simple no-hover" href="https://twitter.com/intent/like?tweet_id=<?php echo $tweet['id_str']; ?>"></a>
			                        <a class="icon-retweet link-simple no-hover" href="https://twitter.com/intent/retweet?tweet_id=<?php echo $tweet['id_str']; ?>"></a>
			                    </div>

			                    <p class="date">
			                        <strong>
										<a class="link-simple" href="https://twitter.com/<?php echo $twitterName; ?>/status/<?php echo $tweet['id_str']; ?>" target="_blank">
							                <?php echo date('M d', strtotime($tweet['created_at'])); ?>
							            </a>
			                        </strong>
			                    </p>
			                </div>
			            </div>
						<?php
					endforeach;
				endif;
				?>
			</div>
		</div>
	</div>
<?php
endif;
?>

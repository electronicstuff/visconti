<?php get_header(); ?>
<section class="container">
    <?php get_template_part( 'templates/coachs', 'intro' ); ?>
    <?php get_template_part( 'templates/coachs', 'filtres' ); ?>
    <?php get_template_part( 'templates/coachs', 'results' ); ?>
    <?php get_template_part( 'templates/general', 'twitter' ); ?>
</section>


<?php

get_footer();
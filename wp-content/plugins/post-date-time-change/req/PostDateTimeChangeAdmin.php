<?php
/**
 * Post Date Time Change
 * 
 * @package    Post Date Time Change
 * @subpackage Post Date Time Change Main & Management screen
/*  Copyright (c) 2014- Katsushi Kawamori (email : dodesyoswift312@gmail.com)
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

class PostDateTimeChangeAdmin {

	/* ==================================================
	 * Add a "Settings" link to the plugins page
	 * @since	1.0
	 */
	function settings_link( $links, $file ) {
		static $this_plugin;
		if ( empty($this_plugin) ) {
			$this_plugin = POSTDATETIMECHANGE_PLUGIN_BASE_FILE;
		}
		if ( $file == $this_plugin ) {
			$links[] = '<a href="'.admin_url('tools.php?page=postdatetimechange').'">'.__( 'Settings').'</a>';
		}
			return $links;
	}

	/* ==================================================
	 * Settings page
	 * @since	1.0
	 */
	function add_pages() {
		add_management_page('Post Date Time Change', 'Post Date Time Change', 'edit_posts', 'postdatetimechange', array($this, 'manage_page'));
	}

	/* =================================================
	 * Add Css and Script
	 * @since	1.0
	 */
	function load_custom_wp_admin_style() {
		if ($this->is_my_plugin_screen()) {
			wp_enqueue_style( 'jquery-responsiveTabs', POSTDATETIMECHANGE_PLUGIN_URL.'/css/responsive-tabs.css' );
			wp_enqueue_style( 'jquery-responsiveTabs-style', POSTDATETIMECHANGE_PLUGIN_URL.'/css/style.css' );
			wp_enqueue_style( 'jquery-datetimepicker', POSTDATETIMECHANGE_PLUGIN_URL.'/css/jquery.datetimepicker.css' );
			wp_enqueue_script( 'jquery' );
			wp_enqueue_script( 'jquery-responsiveTabs', POSTDATETIMECHANGE_PLUGIN_URL.'/js/jquery.responsiveTabs.min.js' );
			wp_enqueue_script( 'jquery-datetimepicker', POSTDATETIMECHANGE_PLUGIN_URL.'/js/jquery.datetimepicker.js', null, '2.3.4' );
		}
	}

	/* ==================================================
	 * Add Script on footer
	 * @since	1.0
	 */
	function load_custom_wp_admin_style2() {
		if ($this->is_my_plugin_screen()) {
			echo $this->add_js();
		}
	}

	/* ==================================================
	 * For only admin style
	 * @since	7.31
	 */
	function is_my_plugin_screen() {
		$screen = get_current_screen();
		if (is_object($screen) && $screen->id == 'tools_page_postdatetimechange') {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/* ==================================================
	 * Main
	 */
	function manage_page() {

		if ( !current_user_can( 'edit_posts' ) )  {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}

		list($page, $readpoststatus, $posttypes, $posttypefilter, $posttypefilter_all, $catfilter, $mimefilter, $sortkey, $searchtext) = $this->query_strings_post_get();

		if( !empty($_POST) ) {
			if ( !empty($_POST['ShowToPage']) ) {
				$this->options_updated();
				echo '<div class="updated"><ul><li>'.__('Settings saved.').'</li></ul></div>';
				$page = 1;
			} else if ( !empty($_POST['UpdateDateTimeChange']) ) {
				list($flag, $keys, $titles, $olddates, $newdates, $error_message) = $this->posts_updated();
				if ( $flag ) {
					foreach ( $keys as $key ) {
						echo '<div class="updated"><ul><li>'.sprintf(__('ID: %1$d Title: %2$s => Changed the date and time to %4$s from %3$s.', 'post-date-time-change'), $key, $titles[$key], $olddates[$key], $newdates[$key]).'</li></ul></div>';
					}
				} else { // error
					if ( !empty($keys) ) {
						foreach ( $keys as $key ) {
							echo '<div class="error"><ul><li>'.sprintf(__('ID: %1$d Title: %2$s => Failed to change the date and time. %3$s', 'post-date-time-change'), $key, $titles[$key], $error_message).'</li></ul></div>';
						}
					}
					echo '<div class="error"><ul><li>'.__('Stoped the process.', 'post-date-time-change').'</li></ul></div>';
				}
			}
		}

		$scriptname = admin_url('tools.php?page=postdatetimechange');

		$pagemax = $this->pagemax_each_user();

		?>
		<div class="wrap">
		<h2>Post Date Time Change</h2>
			<div id="postdatetimechange-tabs">
				<ul>
				<li><a href="#postdatetimechange-tabs-1"><?php _e('Edit date and time'); ?></a></li>
				<li><a href="#postdatetimechange-tabs-2"><?php _e('Donate to this plugin &#187;'); ?></a></li>
				</ul>
				<div id="postdatetimechange-tabs-1">

			<?php
			echo '<div id="postdatetimechange-loading" style="position: relative; left: 40%; top: 10%;"><img src="'.POSTDATETIMECHANGE_PLUGIN_URL.'/css/loading.gif"></div>';
			echo '<div id="postdatetimechange-loading-container">';
			?>

			<form method="post" action="<?php echo $scriptname; ?>">

			<div style="padding-top: 5px; padding-bottom: 5px;">
			 	<input type="submit" name="UpdateDateTimeChange" class="button-primary button-large" value="<?php _e('Change the date and time', 'post-date-time-change') ?>" />
			</div>

			<p>
			<div style="float:left;"><?php _e('Number of items per page:'); ?><input type="text" name="postdatetimechange_settings_pagemax" value="<?php echo $pagemax; ?>" size="3" /></div>
			<input type="submit" class="button" name="ShowToPage" value="<?php _e('Save') ?>" />
			<div style="clear:both"></div>

			<div>
			<select name="poststatus">
			<?php
			$selectedpublish = NULL;
			$selecteddraft = NULL;
			$selectedattach = NULL;
			if ( $readpoststatus === 'publish' ) {
				$selectedpublish = ' selected';
			} else if ( $readpoststatus === 'draft' ) {
				$selecteddraft = ' selected';
			} else if ( $readpoststatus === 'attachment' ) {
				$selectedattach = ' selected';
			}
			?>
			<option value="publish"<?php echo $selectedpublish; ?>><?php echo __('Posts').'-'.__('Publish'); ?></option>
			<option value="draft"<?php echo $selecteddraft; ?>><?php echo __('Posts').'-'.__('Draft'); ?></option>
			<option value="attachment"<?php echo $selectedattach; ?>><?php _e('Media'); ?></option>
			</select>
			<input type="submit" name="poststatusapply" class="button" value="<?php _e('Apply'); ?>">
			<span style="margin-right: 1em;"></span>
			<?php
			if ( $readpoststatus === 'attachment' ) {
				$postpages = $this->db_load_attach($mimefilter, $sortkey, $searchtext);
				?>
				<select name="mime" style="width: 180px;">
				<option value=""><?php echo esc_attr( __( 'All Mime types', 'post-date-time-change' ) ); ?></option> 
				<?php
				global $user_ID;
				$mimes = get_allowed_mime_types($user_ID);
				foreach ( $mimes as $exts => $mime ) {
					?>
					<option value="<?php echo esc_attr($mime); ?>"<?php if ($mimefilter === $mime) echo 'selected';?>><?php echo esc_attr($mime); ?></option>
					<?php
				}
				?>
				</select>
				<?php
			} else {
				$postpages = $this->db_load_post($readpoststatus, $posttypefilter, $posttypefilter_all, $catfilter, $sortkey, $searchtext);
				$catargs = array(
					'show_option_all' => __('All categories'),
					'hierarchical' => 1,
					'selected' => $catfilter
				);
				?>
				<select name="posttype">
					<option value="<?php echo $posttypefilter_all; ?>"<?php if ($posttypefilter === $posttypefilter_all) echo ' selected'; ?>><?php esc_attr(_e('All Types')); ?></option>
				<?php
				foreach ( $posttypes as $key => $value ) {
					?>
					<option value="<?php echo $key; ?>"<?php if ($posttypefilter === $key) echo ' selected'; ?>><?php echo esc_attr($value); ?></option>
					<?php
				}
				?>
				</select>
				<?php
				wp_dropdown_categories($catargs);
			}
			if ( empty($searchtext) ) {
				?>
				<input name="searchtext" type="text" value="" placeholder="<?php echo __('Search'); ?>">
				<?php
			} else {
				?>
				<input name="searchtext" type="text" value="<?php echo $searchtext; ?>">
				<?php
			}
			?>
			<input type="submit" class="button" value="<?php _e('Filter'); ?>">
			</div>
			<?php

			$pageallcount = 0;
			// pagenation
			foreach ( $postpages as $postpage ) {
				++$pageallcount;
			}
			$pagelast = ceil($pageallcount / $pagemax);
			if ( $page <= 0 ) {
				$page = 1;
			} else if ( $page > $pagelast ) {
				$page = $pagelast;
			}
			$pagebegin = (($page - 1) * $pagemax) + 1;
			$pageend = $page * $pagemax;
			$count = 0;

			$this->pagenation($page, $pagebegin, $pageend, $pagelast, $pageallcount, $scriptname, $readpoststatus, $posttypefilter, $catfilter, $mimefilter, $sortkey, $searchtext, 'jump');
			if ( $pagelast > 0 ) {
				?>
				<div style="border-bottom: 1px solid; padding-top: 5px; padding-bottom: 5px;">
					<div>
					<?php
					if ( $sortkey === 'DESC' ) {
						$newsortkey = 'ASC';
						$newsort_text = __('Ascending');
						$dashiconname = 'dashicons-arrow-up';
					} else {
						$newsortkey = 'DESC';
						$newsort_text = __('Descending');
						$dashiconname = 'dashicons-arrow-down';
					}
					$scriptname_plusarg = add_query_arg( array('p' => $page, 'poststatus' => $readpoststatus, 'posttype' => $posttypefilter, 'cat' => $catfilter, 'mime' => $mimefilter, 'sort' => $newsortkey, 'searchtext' => $searchtext ),  $scriptname);
					?>
					<a title="<?php echo $newsort_text; ?>" href="<?php echo $scriptname_plusarg; ?>" style="text-decoration: none; display:inline-block; vertical-align:middle;"> <span class="dashicons <?php echo $dashiconname; ?>"></span><?php echo $newsort_text; ?></a>
					</div>
					<input type="hidden" name="sort" value="<?php echo $sortkey; ?>">
				</div>
				<?php
			}

			if ($postpages) {
				foreach ( $postpages as $postpage ) {
					++$count;
					if ( $pagebegin <= $count && $count <= $pageend ) {
						$postid = $postpage->ID;
						$title = $postpage->post_title;
						$link = get_permalink($postpage->ID);
						$thumb_html = NULL;
						if ( $readpoststatus === 'attachment' ) {
							$link = get_attachment_link($postpage->ID);
							$image_attr_thumbnail = wp_get_attachment_image_src($postpage->ID, 'thumbnail', true);
							$thumb_html = '<img width="40" height="40" src="'.$image_attr_thumbnail[0].'" align="middle" style="float: left; margin: 5px;">';
							$poststatus = $postpage->post_mime_type;
						}
						if ( $readpoststatus <> 'attachment' ) {
							$posttypeobj = get_post_type_object($postpage->post_type);
							$poststatus = $posttypeobj->labels->name;
							if ( $postpage->post_type === 'page' ) {
								$thumb_html = '<span class="dashicons dashicons-admin-page" style="float: left; margin: 5px;"></span>';
							} else {
								$thumb_html = '<span class="dashicons dashicons-admin-post" style="float: left; margin: 5px;"></span>';
							}
						}
						$date = $postpage->post_date;
						$newdate = substr( $date , 0 , strlen($date)-3 );
					?>
					<div style="border-bottom: 1px solid; padding-top: 5px; padding-bottom: 5px;">
						<?php echo $thumb_html; ?>
						<div style="overflow: hidden;">
						<div>ID: <?php echo $postpage->ID; ?></div>
						<div><?php _e('Title'); ?>: <?php echo $title; ?></div>
						<div><?php _e('Permalink:'); ?> <a style="color: #4682b4;" title="<?php _e('View');?>" href="<?php echo $link; ?>" target="_blank"><?php echo $link; ?></a></div>
						<div><?php _e('Users'); ?>: <?php echo get_the_author_meta('display_name', $postpage->post_author); ?></div>
						<div>
						<?php
						if ( $readpoststatus <> 'attachment' ) {
							_e('Type'); ?>: 
						<?php
						} else {
							_e('Mime type', 'post-date-time-change'); ?>: 
						<?php
						}
						echo $poststatus; ?>
						</div>
						<div><?php _e('Date/Time'); ?></div>
						<div style="padding: 0px 10px;">
						<div><?php _e('Current', 'post-date-time-change'); ?>: <?php echo $date; ?></div>
						<div><?php _e('Edit'); ?>: <input type="text" id="datetimepicker-postdatetimechange<?php echo $postid; ?>" name="postdatetimechange_datetime[<?php echo $postid ?>]" value="<?php echo $newdate; ?>" /></div>
						</div>
						</div>
						<div style="clear:both"></div>
					</div>
					<?php
					}
				}
			}

			?>
				<?php
				$this->pagenation($page, $pagebegin, $pageend, $pagelast, $pageallcount, $scriptname, $readpoststatus, $posttypefilter, $catfilter, $mimefilter, $sortkey, $searchtext, NULL);
				?>

			<div style="padding-top: 15px; padding-bottom: 5px;">
				<input type="submit" name="UpdateDateTimeChange" class="button-primary button-large" value="<?php _e('Change the date and time', 'post-date-time-change') ?>" />
			</div>

			</form>

			</div>

		</div>

		<div id="postdatetimechange-tabs-2">

		<?php
		$plugin_datas = get_file_data( POSTDATETIMECHANGE_PLUGIN_BASE_DIR.'/postdatetimechange.php', array('version' => 'Version') );
		$plugin_version = __('Version:').' '.$plugin_datas['version'];
		?>
		<div class="wrap">
		<h4 style="margin: 5px; padding: 5px;">
		<?php echo $plugin_version; ?> |
		<a style="text-decoration: none;" href="https://wordpress.org/support/plugin/post-date-time-change" target="_blank"><?php _e('Support Forums') ?></a> |
		<a style="text-decoration: none;" href="https://wordpress.org/support/view/plugin-reviews/post-date-time-change" target="_blank"><?php _e('Reviews', 'post-date-time-change') ?></a>
		</h4>
		<div style="width: 250px; height: 180px; margin: 5px; padding: 5px; border: #CCC 2px solid;">
		<h3><?php _e('Please make a donation if you like my work or would like to further the development of this plugin.', 'post-date-time-change'); ?></h3>
		<div style="text-align: right; margin: 5px; padding: 5px;"><span style="padding: 3px; color: #ffffff; background-color: #008000">Plugin Author</span> <span style="font-weight: bold;">Katsushi Kawamori</span></div>
		<a style="margin: 5px; padding: 5px;" href='https://pledgie.com/campaigns/28307' target="_blank"><img alt='Click here to lend your support to: Various Plugins for WordPress and make a donation at pledgie.com !' src='https://pledgie.com/campaigns/28307.png?skin_name=chrome' border='0' ></a>
		</div>

		</div>

		</div>
		<?php
	}

	/* ==================================================
	 * Pagenation
	 * @since	1.0
	 * string	$page
	 * string	$pagebegin
	 * string	$pageend
	 * string	$pagelast
	 * string	$pageallcount
	 * string	$scriptname
	 * string	$readpoststatus
	 * string	$posttypefilter
	 * string	$catfilter
	 * string	$mimefilter
	 * string	$sortkey
	 * string	$searchtext
	 * string	$flag
	 * return	$html
	 */
	function pagenation($page, $pagebegin, $pageend, $pagelast, $pageallcount, $scriptname, $readpoststatus, $posttypefilter, $catfilter, $mimefilter, $sortkey, $searchtext, $flag){

		$pageprev = $page - 1;
		$pagenext = $page + 1;
		$scriptnamefirst = add_query_arg( array('p' => '1', 'poststatus' => $readpoststatus, 'posttype' => $posttypefilter, 'cat' => $catfilter, 'mime' => $mimefilter, 'sort' => $sortkey, 'searchtext' => $searchtext ),  $scriptname);
		$scriptnameprev = add_query_arg( array('p' => $pageprev, 'poststatus' => $readpoststatus, 'posttype' => $posttypefilter, 'cat' => $catfilter, 'mime' => $mimefilter, 'sort' => $sortkey, 'searchtext' => $searchtext ),  $scriptname);
		$scriptnamenext = add_query_arg( array('p' => $pagenext, 'poststatus' => $readpoststatus, 'posttype' => $posttypefilter, 'cat' => $catfilter, 'mime' => $mimefilter, 'sort' => $sortkey, 'searchtext' => $searchtext ),  $scriptname);
		$scriptnamelast = add_query_arg( array('p' => $pagelast, 'poststatus' => $readpoststatus, 'posttype' => $posttypefilter, 'cat' => $catfilter, 'mime' => $mimefilter, 'sort' => $sortkey, 'searchtext' => $searchtext ),  $scriptname);

		if ( $pagelast == 0 ) {
			if ( $flag === 'jump' ) {
				echo '<div class="updated"><ul><li>'.__('No items.').'</li></ul></div>';
			}
		} else {
			if ( $flag === 'jump' ) {
				?>
				<div style="width: 100%; text-align: center; margin: 20px 0px 0px;">
				<?php echo sprintf(__('%s item'), $pageallcount);
				?>
				</div>
				<?php
			} else {
				?>
				<div style="width: 100%; text-align: center; margin: 10px 0px 0px;">
				<?php echo sprintf(__('%s item'), $pageallcount);
				?>
				</div>
				<?php
			}
		}
		if ( $pagelast > 1 ) {
			if ( $flag === 'jump' ) {
				?>
				<div style="width: 100%; text-align: center; margin: 0px 0px 10px;">
				<?php
			} else {
				?>
				<div style="width: 100%; text-align: center; margin: 15px 0px 20px;">
				<?php
			}
			$a_style = 'style="padding: 13px; font-size: 13px; text-decoration: none; background: #d3d3d3;" onmouseover="this.style.backgroundColor=&#39;#ffffff&#39;" onmouseout="this.style.backgroundColor=&#39;#d3d3d3&#39;"';
			?>
			<span style="margin-right: 0.5em;"></span>
			<?php
			if ( $page <> 1 ){
				?><a <?php echo $a_style; ?> title='<?php _e('Go to the first page', 'post-date-time-change'); ?>' href='<?php echo $scriptnamefirst; ?>'>&laquo;</a>
				<a <?php echo $a_style; ?> title='<?php _e('Go to the previous page', 'post-date-time-change'); ?>' href='<?php echo $scriptnameprev; ?>'>&lsaquo;</a>
			<?php
			}
			echo $page;
			?>/
			<?php echo $pagelast;
			if ( $page <> $pagelast ){
				?>&nbsp;<a <?php echo $a_style; ?> title='<?php _e('Go to the next page', 'post-date-time-change'); ?>' href='<?php echo $scriptnamenext; ?>'>&rsaquo;</a>
				<a <?php echo $a_style; ?> title='<?php _e('Go to the last page', 'post-date-time-change'); ?>' href='<?php echo $scriptnamelast; ?>'>&raquo;</a>
			<?php
			}
			if ( $flag === 'jump' ) {
				?>
				<input type="text" name="postdatetimechange_page" value="<?php echo $page; ?>" size="3" style="padding: 13px; font-size: 13px;" /><button type="submit" style="padding: 10px; font-size: 13px; vertical-align: 2px; color: #00abd2; background: #d3d3d3;" onmouseover="this.style.backgroundColor='#ffffff'" onmouseout="this.style.backgroundColor='#d3d3d3'" title="<?php _e('jump', 'post-date-time-change'); ?>"><span class="dashicons dashicons-leftright"></span></button>
				<?php
			}
			?>
			</div>
			<div style="clear:both"></div>
			<?php
		}

	}

	/* ==================================================
	 * @param	none
	 * @return	array	$posttypes
	 * @since	2.85
	 */
	function search_posttype(){

		$args = array(
		   'public'   => true,
		   '_builtin' => false
		);
		$custom_post_types = get_post_types( $args, 'objects', 'and' ); 
		foreach ( $custom_post_types as $post_type ) {
			$posttypes[$post_type->name] = $post_type->label;
		}

		$posttypes['post'] = __('Posts');
		$posttypes['page'] = __('Pages');

		return $posttypes;

	}

	/* ==================================================
	 * Update wp_options table.
	 * @since	1.0
	 */
	function options_updated(){

		$user = wp_get_current_user();
		$userid = $user->ID;
		$wp_options_name = 'postdatetimechange_settings'.'_'.$userid;
		$settings_tbl = array(
						'pagemax' => intval($_POST['postdatetimechange_settings_pagemax'])
						);
		update_option( $wp_options_name, $settings_tbl );

	}

	/* ==================================================
	 * Update wp_post table.
	 * @since	1.0
	 */
	function posts_updated(){

		if(isset($_POST['postdatetimechange_datetime'])){ $postdatetimechange_datetimes = $_POST['postdatetimechange_datetime']; }

		if ( !empty($postdatetimechange_datetimes) ) {

			$upload_dir = wp_upload_dir();

			$keys = array();
			$titles = array();
			$olddates = array();
			$newdates = array();

			foreach ( $postdatetimechange_datetimes as $key => $value ) {
				$postdate = $value.':00';
				$postdategmt = get_gmt_from_date($postdate);
				$post_datas = get_post ( $key );
				$new_subdir = FALSE;
				if ( $post_datas->post_type === 'attachment' && get_option( 'uploads_use_yearmonth_folders' ) ) {
					$y = substr( $postdategmt, 0, 4 );
					$m = substr( $postdategmt, 5, 2 );
					$subdir = "/$y/$m";

					$post_data_date_gmt = $post_datas->post_date_gmt;
					$y_post = substr( $post_data_date_gmt, 0, 4 );
					$m_post = substr( $post_data_date_gmt, 5, 2 );
					$subdir_post = "/$y_post/$m_post";

					if ( $subdir <> $subdir_post ) { $new_subdir = TRUE; }
				}

				$err_mkdir = TRUE;
				$err_copy = TRUE;
				$copy_file_org = NULL;
				$copy_file_new = NULL;
				if ( $new_subdir ) {
					$metaolddata = array();
					$metaolddata = wp_get_attachment_metadata( $key );
					$new_file = $subdir.'/'.wp_basename($metaolddata['file']);
					$newurl = $upload_dir['baseurl'].$new_file;
					$new_file = substr( $new_file, 1 );

					$filename = wp_basename($new_file);
					$suffix_new_files = explode('.', $new_file);
					$ext = end($suffix_new_files);

					$upload_new_path = $upload_dir['basedir'].$subdir;
					$upload_old_path = $upload_dir['basedir'].$subdir_post;
					if ( !file_exists($upload_new_path) ) {
						$err_mkdir = @wp_mkdir_p($upload_new_path);
						if ( !$err_mkdir ) {
							$keys[] = $key;
							$titles[$key] = $post_datas->post_title;
							$olddates[$key] = $post_datas->post_date;
							$newdates[$key] = $postdate;
							return array(FALSE, $keys, $titles, NULL, NULL, sprintf(__('Unable to create directory [%1$s].', 'post-date-time-change'), wp_normalize_path($upload_new_path)));
						}
					}
					$copy_file_org = $upload_old_path.'/'.$filename;
					$copy_file_new = $upload_new_path.'/'.$filename;
					$err_copy = @copy( $copy_file_org, $copy_file_new );
					if ( !$err_copy ) {
						$keys[] = $key;
						$titles[$key] = $post_datas->post_title;
						$olddates[$key] = $post_datas->post_date;
						$newdates[$key] = $postdate;
						return array(FALSE, $keys, $titles, NULL, NULL, sprintf(__('Failed a copy from %1$s to %2$s.', 'post-date-time-change'), wp_normalize_path($copy_file_org), wp_normalize_path($copy_file_new)));
					}

					update_attached_file( $key, $new_file );

					if ( wp_ext2type($ext) === 'image' ){
						$oldthumbs = array();
						foreach ( $metaolddata as $key1 => $key2 ){
							if ( $key1 === 'sizes' ) {
								foreach ( $metaolddata[$key1] as $key2 => $key3 ){
									$oldthumbs[] = $upload_old_path.'/'.$metaolddata['sizes'][$key2]['file'];
								}
							}
						}
						foreach ( $oldthumbs as $oldthumb ) {
							unlink( $oldthumb );
						}
					    unlink( $upload_old_path.'/'.$filename );
						$metadata = wp_generate_attachment_metadata( $key, $upload_new_path.'/'.$filename );
						wp_update_attachment_metadata( $key, $metadata );
					}else if ( wp_ext2type($ext) === 'video' ){
						$metadata = wp_read_video_metadata( $upload_new_path.'/'.$filename );
						wp_update_attachment_metadata( $key, $metadata );
					    unlink( $upload_old_path.'/'.$filename );
					}else if ( wp_ext2type($ext) === 'audio' ){
						$metadata = wp_read_audio_metadata( $upload_new_path.'/'.$filename );
						wp_update_attachment_metadata( $key, $metadata );
					    unlink( $upload_old_path.'/'.$filename );
					} else {
						$metadata = NULL;
					    unlink( $upload_old_path.'/'.$filename );
					}

					$up_post = array(
									'ID' => $key,
									'post_date' => $postdate,
									'post_date_gmt' => $postdategmt,
									'post_modified' => $postdate,
									'post_modified_gmt' => $postdategmt,
									'guid' => $newurl
								);
					wp_update_post( $up_post );
					$keys[] = $key;
					$titles[$key] = $post_datas->post_title;
					$olddates[$key] = $post_datas->post_date;
					$newdates[$key] = $postdate;
				} else {
					$post_date_org = substr($post_datas->post_date, 0, -3).':00';
					if ( $post_date_org <> $postdate ) {
						$up_post = array(
										'ID' => $key,
										'post_date' => $postdate,
										'post_date_gmt' => $postdategmt,
										'post_modified' => $postdate,
										'post_modified_gmt' => $postdategmt,
										'edit_date' => TRUE
									);
						wp_update_post( $up_post );
						if ( $post_datas->post_type <> 'attachment' ) {
							$rev_post = array(
											'post_author' => $post_datas->post_author,
											'post_date' => $postdate,
											'post_date_gmt' => $postdategmt,
											'post_content' => $post_datas->post_content,
											'post_title' => $post_datas->post_title,
											'post_excerpt' => $post_datas->post_excerpt,
											'post_status' => 'inherit',
											'post_name' => $key.'-revision-from-'.$post_datas->post_date.'-to-'.$postdate,
											'post_modified' => $postdate,
											'post_modified_gmt' => $postdategmt,
											'post_parent' => $key,
											'post_type' => 'revision'
										);
							wp_insert_post( $rev_post );
						}
						$keys[] = $key;
						$titles[$key] = $post_datas->post_title;
						$olddates[$key] = $post_datas->post_date;
						$newdates[$key] = $postdate;
					}
				}
			}
			return array(TRUE, $keys, $titles, $olddates, $newdates, NULL);
		}

	}

	/* ==================================================
	 * Data Base Load Posts
	 * @since	3.0
	 * @param
	 * string	$mimefilter
	 * string	$sortkey
	 * string	$searchtext
	 * @return	array	$postpages
	 */
	function db_load_attach($mimefilter, $sortkey, $searchtext) {

		$postmimetype = NULL;
		if ( !empty($mimefilter) ) {
			$postmimetype = "AND post_mime_type = '".$mimefilter."'";
		}

		$postauthor = NULL;
		if ( !current_user_can('administrator') ) {
			$user = wp_get_current_user();
			$postauthor = "AND post_author = '".$user->ID."'";
		}

		global $wpdb;
		$postpages = $wpdb->get_results("
						SELECT	ID, post_author, post_title, post_date, post_mime_type
						FROM	$wpdb->posts
						WHERE	post_type = 'attachment'
								AND post_title LIKE '%%$searchtext%%'
								$postmimetype
								$postauthor
								ORDER BY post_date $sortkey
						");

		return array_unique($postpages, SORT_REGULAR);

	}

	/* ==================================================
	 * Data Base Load Posts
	 * @since	3.0
	 * @param
	 * string	$readpoststatus
	 * string	$posttypefilter
	 * string	$posttypefilter_all
	 * int		$catfilter
	 * string	$sortkey
	 * string	$searchtext
	 * @return	array	$postpages
	 */
	function db_load_post($readpoststatus, $posttypefilter, $posttypefilter_all, $catfilter, $sortkey, $searchtext) {

		$query_posttypefilter = NULL;
		if( $posttypefilter === $posttypefilter_all ) {
			$posttypefilters = explode(',', $posttypefilter);
			foreach ( $posttypefilters as $key ) {
				$query_posttypefilter .= "'".$key."',";
			}
			$query_posttypefilter = rtrim($query_posttypefilter, ',');
		} else {
			$query_posttypefilter = "'".$posttypefilter."'";
		}

		$postauthor = NULL;
		if ( !current_user_can('administrator') ) {
			$user = wp_get_current_user();
			$postauthor = "AND p.post_author = '".$user->ID."'";
		}

		$query_catfilter = NULL;
		if ( $catfilter <> 0 ) {
			$query_catfilter = 'AND r.term_taxonomy_id = %d';
		}

		global $wpdb;
		$postpages = $wpdb->get_results($wpdb->prepare("
						SELECT	p.ID, p.post_author, p.post_title, p.post_status, p.post_type, p.post_date
						FROM 	$wpdb->posts p
						LEFT	JOIN $wpdb->term_relationships r
								ON p.ID = r.object_id
								WHERE p.post_type IN ($query_posttypefilter)
								AND p.post_status = %s
								AND p.post_title LIKE '%%$searchtext%%'
								$postauthor
								$query_catfilter
								ORDER BY p.post_date $sortkey
							",$readpoststatus,$catfilter
							));

		return array_unique($postpages, SORT_REGULAR);

	}

	/* ==================================================
	 * Query Strings Post & Get
	 * @since	3.0
	 * @param	none
	 * @return
	 * string	$page
	 * string	$readpoststatus
	 * array	$posttypes
	 * string	$posttypefilter
	 * string	$posttypefilter_all
	 * string	$catfilter
	 * string	$mimefilter
	 * string	$sortkey
	 * string	$searchtext
	 */
	function query_strings_post_get(){

		if ( !empty($_POST['postdatetimechange_page']) ) {
			$page = intval($_POST['postdatetimechange_page']);
		} else {
			if (!empty($_GET['p'])){
				$page = $_GET['p'];
			} else {
				$page = 1;
			}
		}
		if ( !empty($_POST['poststatusapply']) ) {
			$page = 1;
		}
		$readpoststatus = 'publish';
		if( !empty($_GET['poststatus']) ) {
			$readpoststatus = $_GET['poststatus'];
		}
		if( !empty($_POST['poststatus']) ) {
			$readpoststatus = $_POST['poststatus'];
		}

		$posttypefilter = NULL;
		$posttypes = $this->search_posttype();
		foreach ( $posttypes as $key => $value ) {
			$posttypefilter .= $key.',';
		}
		$posttypefilter_all = rtrim($posttypefilter, ',');
		$posttypefilter = $posttypefilter_all;

		$catfilter = 0;
		$mimefilter = NULL;
		$sortkey = 'DESC';
		$searchtext = NULL;
		if( !empty($_GET['posttype']) ) {
			$posttypefilter = $_GET['posttype'];
		}
		if( !empty($_POST['posttype']) ) {
			$posttypefilter = $_POST['posttype'];
		}
		if( !empty($_GET['cat']) ) {
			$catfilter = $_GET['cat'];
		}
		if( !empty($_POST['cat']) ) {
			$catfilter = $_POST['cat'];
		}
		if( !empty($_GET['mime']) ) {
			$mimefilter = $_GET['mime'];
		}
		if( !empty($_POST['mime']) ) {
			$mimefilter = $_POST['mime'];
		}
		if (!empty($_GET['sort'])){
			$sortkey = $_GET['sort'];
		}
		if( !empty($_POST['sort']) ) {
			$sortkey = $_POST['sort'];
		}
		if ( !empty($_POST['searchtext']) ) {
			$searchtext = $_POST['searchtext'];
		} else if ( !empty($_GET['searchtext']) ) {
			$searchtext = $_GET['searchtext'];
		}
		if ( !empty($searchtext) ) {
			$searchtext = $this->mb_utf8($searchtext);
		}

		return array($page, $readpoststatus, $posttypes, $posttypefilter, $posttypefilter_all, $catfilter, $mimefilter, $sortkey, $searchtext);

	}

	/* ==================================================
	 * @param	string	$str
	 * @return	string	$str
	 * @since	3.0
	 */
	function mb_utf8($str) {

		if ( function_exists('mb_convert_encoding') ) {
			$str = mb_convert_encoding($str, "UTF-8", "auto");
		}

		return $str;

	}

	/* ==================================================
	 * Add js
	 * @since	1.0
	 */
	function add_js(){

		list($page, $readpoststatus, $posttypes, $posttypefilter, $posttypefilter_all, $catfilter, $mimefilter, $sortkey, $searchtext) = $this->query_strings_post_get();

// JS
$postdatetimechange_add_js = <<<POSTDATETIMECHANGE1

<!-- BEGIN: Post Date Time Change -->
<script type="text/javascript">
jQuery('#postdatetimechange-tabs').responsiveTabs({
  startCollapsed: 'accordion'
});
</script>
<script type="text/javascript">
POSTDATETIMECHANGE1;

		if ( $readpoststatus === 'attachment' ) {
			$postpages = $this->db_load_attach($mimefilter, $sortkey, $searchtext);
		} else {
			$postpages = $this->db_load_post($readpoststatus, $posttypefilter, $posttypefilter_all, $catfilter, $sortkey, $searchtext);
		}

		$pagemax = $this->pagemax_each_user();

		$count = 0;
		$pagebegin = (($page - 1) * $pagemax) + 1;
		$pageend = $page * $pagemax;

		if ($postpages) {
			foreach ( $postpages as $postpage ) {
				++$count;
				if ( $pagebegin <= $count && $count <= $pageend ) {
					$postid = $postpage->ID;
$postdatetimechange_add_js .= <<<POSTDATETIMECHANGE2

jQuery('#datetimepicker-postdatetimechange
POSTDATETIMECHANGE2;
					$postdatetimechange_add_js .= $postid;
$postdatetimechange_add_js .= <<<POSTDATETIMECHANGE3
').datetimepicker({format:'Y-m-d H:i'});
POSTDATETIMECHANGE3;
				}
			}
		}

$postdatetimechange_add_js .= <<<POSTDATETIMECHANGE4

</script>
<script type="text/javascript">
window.addEventListener( "load", function(){
  jQuery("#postdatetimechange-loading").delay(2000).fadeOut();
  jQuery("#postdatetimechange-loading-container").delay(2000).fadeIn();
}, false );
</script>
<script type="text/javascript">
jQuery('input[type!="submit"][type!="button"]').keypress(function(e){
  if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
    return false;
  }else{
    return true;
  }
});
</script>
<!-- END: Post Date Time Change -->

POSTDATETIMECHANGE4;

		return $postdatetimechange_add_js;

	}

	/* ==================================================
	 * Each user settings for pagemax
	 * @param	none
	 * @return	$pagemax
	 * @since	3.02
	 */
	function pagemax_each_user(){

		$user = wp_get_current_user();
		$userid = $user->ID;
		$wp_options_name = 'postdatetimechange_settings'.'_'.$userid;
		$postdatetimechange_settings = get_option($wp_options_name);
		$pagemax = $postdatetimechange_settings['pagemax'];

		return $pagemax;

	}

}

?>
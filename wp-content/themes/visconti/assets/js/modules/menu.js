(function(){

    window.visconti = window.visconti || {};
    window.visconti.menu = menu;

    function menu(){
        genericMenu();
        mobileMenu();
        search();
    }

    function search(){
        var $search = $('.search-container');
        var $open = $('.js-search');
        var $close = $search.find('.js-close');

        if($search.length && $open.length && $close.length){
            $open.on('click', function(){
                $search.slideDown();
            });
            $close.on('click', function(){
                $search.slideUp();
            });
        }
    }

    function mobileMenu(){
        var $burger = $('.js-toggle-menu');
        var $togglers = $('.js-mobile-menu');
        var $body = $(document.body);

        if($togglers.length){
            $togglers.on('click', '.toggler', function(e){
                var parent = $(e.delegateTarget);

                e.preventDefault();
                parent.toggleClass('open');
            });
        }
        if($burger.length){
            $burger.on('click', function(){
                $body.toggleClass('mobile-open');
            });
        }
    }

    function genericMenu(){
        var $menu = $('.js-menu');

        if($menu.length){
            $menu.on('click', '.js-open', function(e){
                e.preventDefault();

                $menu.addClass('open');
            });
        }
    }

})();
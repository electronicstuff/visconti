(function () {

    window.visconti = window.visconti || {};
    window.visconti.background = background;

    var $mainWrapper;
    var $mainContainer;

    var topInstance;
    var midInstance;

    function background() {
        $mainWrapper = $('.main-wrapper');
        $mainContainer = $('section.home-container');

        topInstance = topBackground();
        midInstance = midBackground();

        if(topInstance && midInstance){
            topInstance();
            midInstance();
            event();
        }
    }

    function event() {
        $(window).on('resize', function () {
            topInstance();
            midInstance();
        });
    }

    function topBackground() {
        var $element;
        var $challenging = $('.section.challenging');

        if (!$challenging.length) {
            return false;
        }

        $element = $('<div>', {
            'class': 'background-deco top'
        });
        $mainWrapper.prepend($element);

        return function () {
            var mainTop = $mainWrapper.offset().top;
            var top = $challenging.offset().top;
            var height = $challenging.height();

            $element.css('height', top - mainTop + height / 2);
        };

    }

    function midBackground() {
        var $element;

        var $beginning = $('.section.experience');
        var $end = $beginning.next();

        if (!$beginning.length) {
            return false;
        }

        $element = $('<div>', {
            'class': 'background-deco mid'
        });
        $mainContainer.prepend($element);

        return function () {
            var mainTop = $mainContainer.offset().top;
            var beginningTop = $beginning.offset().top;
            var endTop = $end.offset().top;
            var beginningHeight = $beginning.height();
            var endHeight = $end.height();
            var elTop = beginningTop - mainTop + beginningHeight/2;
            var elBottom = endTop - mainTop + endHeight/2;

            $element.css({
                'top' : elTop,
                'height' : elBottom - elTop,
                'left' : '-50px',
                'right' : '-50px'
            });
        };
    }

})();
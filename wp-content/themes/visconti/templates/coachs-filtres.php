<?php
$lang = $_GET['lang'];
?>

<div class="coachs-filters">
    <div class="title">
        <strong>
            <?php echo __('Affiner votre recherche', 'vlang'); ?>
        </strong>
    </div>

    <a class="hide-desktop filters-burger">
        <i class="icon-bars"></i>
        <?php echo __('Filtrer par', 'vlang'); ?>
    </a>
    <div class="filters-container">
        <form method="get" class="search-taxo-form filters">
            <?php if($lang):
                $param = 'lang';
            ?>
                <input hidden name="<?php echo $param; ?>" value="<?php echo $lang; ?>" />
            <?php endif; ?>
            <div class="row">
                <p class="hide-mobile hide-tablet">
                    <?php echo __('Filtrer par', 'vlang'); ?>
                </p>
                <div class="form-round first trigger-select">
                    <i class="icon-caret-down caret"></i>

                    <select name="coach_lang">
                        <option value=""><?php echo __( 'Langue', 'vlang' ); ?></option>
                        <?php generate_taxo_input( 'coach_lang' ); ?>
                    </select>
                </div>

                <div class="form-round trigger-select">
                    <i class="icon-caret-down caret"></i>

                    <select name="coach_needs">
                        <option value=""><?php echo __( 'Secteur', 'vlang' ); ?></option>
                        <?php generate_taxo_input( 'coach_needs' ); ?>
                    </select>
                </div>

                <div class="form-round trigger-select">
                    <i class="icon-caret-down caret"></i>

                    <select name="coach_city">
                        <option value=""><?php echo __( 'Ville', 'vlang' ); ?></option>
                        <?php generate_taxo_input( 'coach_city' ); ?>
                    </select>
                </div>

                <p class="hide-mobile hide-tablet">
                    <?php echo __('ou', 'vlang'); ?>
                </p>

                <div class="form-round no-wrap">
                    <i class="icon-search"></i>
                    <input value="<?php echo $_GET['title']?>" name="title" type="text" placeholder="<?php echo __('Rechercher par nom', 'vlang'); ?>" class="raw"/>
                </div>
            </div>

            <div class="search">
                <button type="submit" href="#" class="link-simple rounded accent icon">
                    <?php echo __('Rechercher', 'vlang'); ?>
                    <i class="icon-arrow-right"></i>
                </button>
            </div>
        </form>
    </div>
</div>


<?php /*
<div id="main-content" class="main-content">
    <form action="/" method="get" class="search-taxo-form">
        <?php generate_taxo_input( 'coach_lang' ); ?>
        <?php generate_taxo_input( 'coach_needs' ); ?>
        <?php generate_taxo_input( 'coach_city' ); ?>
        <button type="submit">Submit</button>
    </form>
    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">

        <?php
        if(have_posts()) : while(have_posts()) : the_post();
        ?>
            <li id="post-<?php the_ID(); ?>">
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </li>
        <?php
        endwhile; endif;
        ?>
</div>
*/ ?>

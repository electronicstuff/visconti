<?php
/*
Plugin Name: Coachs
Plugin URI: -
Description: Adds Custom post type for Coachs
Version: 1.0
Author: -
Author -
License: -
*/


// ----------------------- //
// Adding Custom Post Type //
// ----------------------- //

add_action( 'init', 'coachs_create_post_type' );
function coachs_create_post_type() {
    $labels = array(
        'name'               => __( 'Coachs' ),
        'singular_name'      => __( 'coach' ),
        'add_new'            => __( 'Nouveau Coach' ),
        'add_new_item'       => __( 'Ajouter un Coach' ),
        'edit_item'          => __( 'Editer un Coach' ),
        'new_item'           => __( 'Nouveau Coach' ),
        'view_item'          => __( 'Voir Coach' ),
        'search_items'       => __( 'Rechercher Coach' ),
        'not_found'          =>  __( 'Pas de Coach trouvé' ),
        'not_found_in_trash' => __( 'Pas de Coach trouvé dans la corbeille' ),
    );
    $args = array(
        'labels' => $labels,
        'has_archive' => true,
        'public' => true,
        'hierarchical' => false,
        'supports' => array(
            'title',
            'thumbnail'
        ),
        'taxonomies' => array(''),
        'menu_position' => 4,
    );
    register_post_type( 'coach', $args );
}

// ----------------- //
// Adding Taxonomies //
// ----------------- //

// *** Language *** //
add_action( 'init', 'coachs_register_taxonomy_lang' );
function coachs_register_taxonomy_lang() {
    register_taxonomy( 'coach_lang', array('coach'),
        array(
            'labels' => array(
                'name'              => 'Coachs Langues',
                'singular_name'     => 'Coach Langue',
                'search_items'      => 'Rechercher les Langues de Coachs',
                'all_items'         => 'Tout les Langues de Coachs',
                'edit_item'         => 'Editer les Langues de Coachs',
                'update_item'       => 'Mettre la Langue a jour',
                'add_new_item'      => 'Ajouter une nouvelle Langue',
                'new_item_name'     => 'Voir la Langue',
                'menu_name'         => 'Langues',
            ),
            'hierarchical' => true,
            'sort' => true,
            'args' => array( 'orderby' => 'term_order' ),
            'rewrite' => array( 'slug' => 'coach-lang' ),
            'show_admin_column' => true,
            'query_var' => true
        )
    );
}

// *** Needs *** //
add_action( 'init', 'coachs_register_taxonomy_needs' );
function coachs_register_taxonomy_needs() {
    register_taxonomy( 'coach_needs', array('coach'),
        array(
            'labels' => array(
                'name'              => 'Coachs Besoins',
                'singular_name'     => 'Coach Besoins',
                'search_items'      => 'Rechercher les Besoins de Coachs',
                'all_items'         => 'Tout les Besoins de Coachs',
                'edit_item'         => 'Editer les Besoins de Coachs',
                'update_item'       => 'Mettre la Besoins a jour',
                'add_new_item'      => 'Ajouter une nouvelle Besoins',
                'new_item_name'     => 'Voir la Besoins',
                'menu_name'         => 'Besoins',
            ),
            'hierarchical' => true,
            'sort' => true,
            'args' => array( 'orderby' => 'term_order' ),
            'rewrite' => array( 'slug' => 'coach-needs' ),
            'show_admin_column' => true,
            'query_var' => true
        )
    );
}

// *** City *** //
add_action( 'init', 'coachs_register_taxonomy_city' );
function coachs_register_taxonomy_city() {
    register_taxonomy( 'coach_city', array('coach'),
        array(
            'labels' => array(
                'name'              => 'Coachs Ville',
                'singular_name'     => 'Coach Ville',
                'search_items'      => 'Rechercher les Ville de Coachs',
                'all_items'         => 'Tout les Ville de Coachs',
                'edit_item'         => 'Editer les Ville de Coachs',
                'update_item'       => 'Mettre la Ville a jour',
                'add_new_item'      => 'Ajouter une nouvelle Ville',
                'new_item_name'     => 'Voir la Ville',
                'menu_name'         => 'Ville',
            ),
            'hierarchical' => true,
            'sort' => true,
            'args' => array( 'orderby' => 'term_order' ),
            'rewrite' => array( 'slug' => 'coach-city' ),
            'show_admin_column' => true,
            'query_var' => true
        )
    );
}

// -------------- //
// Template Paths //
// -------------- //

// *** Archive Template *** //
add_filter('template_include', 'coachs_template');
function coachs_template( $template ) {
  if ( is_post_type_archive('coach') ) {
    $theme_files = array('archive-coachs.php', 'coachs-cpt/archive-coachs.php');
    $exists_in_theme = locate_template($theme_files, false);

    if ( $exists_in_theme != '' ) {
      return $exists_in_theme;
    } else {
      return plugin_dir_path(__FILE__) . '/templates/archive-coachs.php';
    }
  }
  return $template;
}

// *** Single Template *** //
add_filter('template_include', 'coach_template');
function coach_template( $template ) {
  if ( is_singular('coach') ) {
    $theme_files = array('single-coach.php', 'coachs-cpt/single-coach.php');
    $exists_in_theme = locate_template($theme_files, false);

    if ( $exists_in_theme != '' ) {
      return $exists_in_theme;
    } else {
      return plugin_dir_path(__FILE__) . '/templates/single-coach.php';
    }
  }
  return $template;
}

// Form //
function generate_taxo_input( $taxonomy ) {
    $queryVar = get_query_var( $taxonomy );
    $terms    = get_terms( $taxonomy, array( "hide_empty" => 0 ) );

    foreach ( $terms as $term ) {
        $slug     = $term->slug;
        $selected = $queryVar == $slug;
        printf( '<option value="%s" %s>%s</option>', $term->slug, $selected ? 'selected' : '', $term->name );
    }
}

// search filter usage:
// /?s=&post_type[]=coach&coach_lang[]=french

// coach archive filter usage:
// /coach/?s=&coach_lang[]=english&coach_lang[]=german&coach_loc[]=france
<?php
/**
 * The template for displaying any single post.
 *
 */

get_header();
$id   = get_the_id();
$cat  = get_the_category($id);
$name = $cat[0]->name;
$url  = 'http://'.$_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
?>

<section>
	<div>
		<?php
		$blogimage = get_field('image_header_blog', 'options');
		$blogimage = wp_get_attachment_image_src( $blogimage, 'page-blog-header');
		$blogimage = $blogimage[0];
		?>
        <div class="blog-header full-background" style="background-image:url('<?php echo $blogimage; ?>');">
            <div class="container flex-center-v">
                <div class="content">
                    <h2 class="subtitle"><?php echo __('L\'actualité Visconti', 'vlang'); ?></h2>
                    <h1 class="title"><?php echo $name ?></h1>
                </div>
            </div>
        </div>
    </div>
	<div class="container">
		<div class="section blog-single">
		    <div class="infos">
		        <div class="back accent-text">
	                <i class="icon-arrow-left"></i>
		            <a href="#" onclick="history.go(-1);return false;" class="link-simple accent-text">
		                <?php echo __('Retour aux actualités', 'vlang'); ?>
		            </a>
		        </div>
		        <div class="summary text-center">
		            <p class="date">
		                <?php echo the_date('d/m/Y'); ?>
		            </p>
		            <p class="zone-title">
		                <?php echo the_title(); ?>
		            </p>
		            <p class="links">
		                <a href="http://twitter.com/share?url=<?php echo $url; ?>" class="link-simple round icon-twitter" target="_blank"></a>
		                <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url; ?>&title=<?php echo the_title(); ?>" class="link-simple round icon-linkedin-square" target="_blank"></a>
		            </p>
		        </div>
		    </div>
		    <div class="content">
				<?php
				$headerImage = get_field('blog_image');
				$imageh      = wp_get_attachment_image_src( $headerImage, 'page-full');
				$imageh      = $imageh[0];
				?>
				<?php if($headerImage): ?>
		        <div class="title-img">
		            <img src="<?php echo $imageh; ?>" alt="">
		        </div>
				<?php endif; ?>
				<div class="wysiwyg-wrapper">
					<?php echo the_content(); ?>
				</div>
		    </div>
			<?php
			$title  = get_field('blog_quote');
			$author = get_field('blog_quote_legend');
			if($title):
			?>
			<div class="section page-quote">
				<div class="quote-wrapper">
					<p class="quote close-quotes">
						<?php echo $title; ?>
					</p>
					<p class="author">
						<?php echo $author; ?>
					</p>
				</div>
			</div>
			<?php endif; ?>
		</div>
		<?php get_template_part( 'templates/home', 'insights' ); ?>
		<?php get_template_part( 'templates/general', 'twitter' ); ?>
	</div>
</section>

<?php get_footer(); ?>

(function(){

    window.visconti = window.visconti || {};
    window.visconti.search = search;

    function search(){
        var $form = $('.search-taxo-form');

        if($form.length){
            $form.on('submit', function(){
                var $this = $(this);
                var serialized = $this.serializeArray();
                var text = $.grep(serialized, function(element){
                    return element.name == 'title';
                })[0];

                if(!text.value){
                    text = null;
                }
                $.each(serialized, function(i, element){
                    if(!element.value || text && element.name !== 'title'){
                        $this.find('[name=' + element.name + ']').removeAttr('name');
                    }
                });
            });
        }
    }

})();
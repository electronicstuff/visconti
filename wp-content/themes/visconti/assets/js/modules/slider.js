(function () {

    window.visconti = window.visconti || {};
    window.visconti.slider = slider;

    var mobile = "screen and (min-width:320px) and (max-width:740px)";
    var tablet = "screen and (min-width:741px) and (max-width:980px)";
    var desktop = "screen and (min-width:981px)";
    var pushSlider;
    var genericInstances = [];

    function slider() {
        homeSlider();
        xpSlider();
        insightsSlider();
        twitterSlider();

        generateGenericSliders();

        registerEvents();
    }

    function homeSlider() {
        var $slider = $('.home-slider');
        var $sliderNav = $slider.children('.swiper-button');
        var $pagination = $slider.children('.swiper-pagination');
        var $texts = [$sliderNav.eq(0).find('span'), $sliderNav.eq(1).find('span')];

        if ($slider.length && $sliderNav.length && $pagination.length) {
            new Swiper($slider, {
                prevButton: $sliderNav.eq(0),
                nextButton: $sliderNav.eq(1),
                pagination: $pagination,
                loop: true,
                onSlideChangeStart: renameDirectionNav
            });
        }

        function renameDirectionNav(slider) {

            var prev;
            var next;
            var destination = slider.activeIndex;
            var total = slider.slides.length;

            prev = destination - 1;
            prev = prev < 0 ? total - 1 : prev;

            next = destination + 1;
            next = next > total - 1 ? 0 : next;

            var slides = slider.slides;
            var prevText = $(slides[prev]).data('cat');
            var nextText = $(slides[next]).data('cat');

            $texts[0].html(prevText);
            $texts[1].html(nextText);
        }
    }

    function insightsSlider() {
        var $wrapper = $('.insights .slider-container');
        var $slider = $('.slider-insights');
        var $sliderNav = $wrapper.find('.button a');
        var $pagination = $slider.children('.swiper-pagination');

        if ($slider.length && $sliderNav.length) {
            new Swiper($slider, {
                prevButton: $sliderNav.eq(0),
                nextButton: $sliderNav.eq(1),
                slidesPerView: 3,
                pagination: $pagination,
                spaceBetween : 40,
                breakpoints: {
                    740: {
                        spaceBetween : 0,
                        slidesPerView: 1,
                        centeredSlides: false
                    },
                    980: {
                        spaceBetween : 30,
                        slidesPerView: 2,
                        centeredSlides: true
                    }
                }
            });
        }
    }

    function xpSlider() {
        var xp = $('.experience');
        var $sliders = xp.find('.xp-slider');
        var $sliderNav = xp.find('.xp-slider-button a');

        if ($sliders.length) {
            $.each($sliders, function (i, slider) {
                var $slider = $(slider);

                new Swiper($slider, {
                    prevButton: $sliderNav.eq(0),
                    nextButton: $sliderNav.eq(1)
                });
            });
        }
    }

    function twitterSlider() {
        var $slider = $('.twitter-slider');

        if ($slider.length) {
            new Swiper($slider, {
                slidesPerView: 5,
                spaceBetween: 5,
                breakpoints: {
                    740: {
                        slidesPerView: 1
                    },
                    980: {
                        slidesPerView: 3
                    }
                }
            });
        }
    }

    function generateGenericSliders(){
        var sliders = ['.offers-slider', '.positionnement-slider', '.col3-slider', '.comite-slider'];

        $.each(sliders, function(i, slider){
            var $slider = $(slider);

            if($slider.length){
                $slider.each(function(i, slider){
                    genericInstances.push(genericSlider($(slider)));
                });
            }
        })
    }
    function runGenericSliders(options){
        $.each(genericInstances, function(i, instance){
            instance(options);
        });
    }

    function genericSlider($parent) {
        var instance;
        var $slider = $parent;
        var $wrapper = $slider.children('.js-push-wrapper');
        var $pagination = $slider.children('.swiper-pagination');
        var $slides = $slider.find('.js-push-item');

        return function (options) {
            var destroy;

            if (options) {
                destroy = options.destroy;
            }

            if (destroy) {
                if (instance) {
                    instance.destroy(true, true);
                    instance = undefined;
                }
                $slides.removeClass('swiper-slide');
                $wrapper.removeClass('swiper-wrapper');
            } else if (!destroy && !instance) {
                if ($slider.length && $pagination.length) {
                    $slides.addClass('swiper-slide');
                    $wrapper.addClass('swiper-wrapper');
                    instance = new Swiper($slider, {
                        pagination: $pagination,
                        breakpoints: {
                            740: {
                                spaceBetween : 30,
                                slidesPerView: 1,
                                centeredSlides: false
                            },
                            980: {
                                spaceBetween : 30,
                                slidesPerView: 2,
                                centeredSlides: true
                            }
                        }
                    });
                }
            }

        };
    }

    pushSlider = (function () {
        var pushSwiper;
        var $slider = $('.push-slider');
        var $pagination = $slider.children('.push-pagination');

        return function (options) {
            var destroy;

            if (options) {
                destroy = options.destroy;
            }

            if (destroy && pushSwiper) {
                pushSwiper.destroy(true, true);
                pushSwiper = undefined;
            } else if (!destroy && !pushSwiper) {
                if ($slider.length && $pagination.length) {
                    pushSwiper = new Swiper($slider, {
                        pagination: $pagination,
                        breakpoints: {
                            740: {
                                spaceBetween : 30,
                                slidesPerView: 1,
                                centeredSlides: false,
                                onTap: function (slider) {
                                    var $current = $(slider.slides[slider.activeIndex]);
                                    var href;

                                    if ($current.parents('.pushes').length) {
                                        href = $current.children('a').first().attr('href');
                                        if (href) {
                                            window.location.href = href;
                                        }
                                    }

                                }
                            },
                            980: {
                                spaceBetween : 30,
                                slidesPerView: 2,
                                centeredSlides: true
                            }
                        }
                    });
                }
            }

        };

    })();

    function registerEvents() {
        enquire
            .register(mobile, function () {
                runGenericSliders();
                pushSlider();
            })
            .register(tablet, function () {
                runGenericSliders();
                pushSlider();
            })
            .register(desktop, function () {
                runGenericSliders({
                    destroy: true
                });
                pushSlider({
                    destroy: true
                });
            });
    }

})();
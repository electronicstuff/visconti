<?php

// Get posts from hightlights //
global $post;
$catName = 'visconti_highlights';
if($_GET['lang']):
    $catName = 'visconti_highlights-'.$_GET['lang'];
endif;

$args = array(
    'category_name' => $catName,
    'posts_per_page' => -1
);
$insight_posts = get_posts($args);
if($insight_posts): ?>
<div class="insights">
	<h2 class="zone-title text-center">
		<?php echo __('Visconti insights', 'vlang'); ?>
	</h2>
	<div class="slider-container">
		<div class="button left">
			<a data-direction="prev" class="link-simple round text-center icon-arrow-left"></a>
		</div>
		<div class="slider-wrapper">
			<div class="swiper-container slider-insights">
				<div class="swiper-wrapper">
                <?php foreach($insight_posts as $post) : setup_postdata($post);?>
                    <fieldset class="insight swiper-slide">
                        <legend><?php echo get_the_date('d/m/Y'); ?></legend>
                        <h4 class="title"><?php echo get_the_title(); ?></h4>
                        <p>
                            <?php echo get_the_excerpt(); ?>
                        </p>
                        <a href="<?php echo get_the_permalink(); ?>" class="link accent-text">
                            <?php echo __('En savoir plus', 'vlang'); ?><i class="icon-arrow-right"></i>
                        </a>
                    </fieldset>
                <?php endforeach; ?>
                </div>
				<div class="swiper-pagination"></div>
			</div>
		</div>
		<div class="button right">
			<a data-direction="next" class="link-simple round text-center icon-arrow-right"></a>
		</div>
	</div>
</div>
<?php endif; ?>

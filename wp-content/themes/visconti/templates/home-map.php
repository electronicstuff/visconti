<?php
$title = get_field('home_map_title');
$subtitle = get_field('home_map_subtitle');
?>
<div class="section challenging">
    <h2 class="zone-title text-center">
        <?php echo $title; ?>
    </h2>
    <h5 class="zone-subtitle text-center">
        <?php echo $subtitle; ?>
    </h5>
    <div class="challenging-container">
        <div class="stats flex-center-v">
            <?php if( have_rows('home_map_values') ): ?>
            <ul class="no-style">
                <?php while( have_rows('home_map_values') ): the_row();
                    $number   = get_sub_field('home_map_value_number');
                    $subtitle = get_sub_field('home_map_value_subtitle');
                ?>
                <li>
                    <span class="number"><?php echo $number; ?></span>
                    <span class="type"><?php echo $subtitle; ?></span>
                </li>
                <?php endwhile; ?>
            </ul>
            <?php endif; ?>
        </div>
        <div class="map" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/img/bg/map.jpg')"></div>
    </div>
</div>
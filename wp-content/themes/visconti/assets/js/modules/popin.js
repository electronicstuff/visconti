(function () {

    window.visconti = window.visconti || {};
    window.visconti.popin = popin;

    function popin() {
        coachPopin();
    }

    function coachPopin() {
        var $trigger = $('.coach-pres .consult');
        var $popin = $('.popin-contact');
        var $close = $popin.find('.close');

        if ($trigger.length && $popin.length && $close.length) {
            $trigger.on('click', function () {
                $popin.css('opacity', 0);
                $popin.removeClass('hidden');
                $popin.animate({'opacity': 1});
            });
            $close.on('click', function () {
                $popin.animate({'opacity': 0}, 500, function () {
                    $popin.addClass('hidden');
                });
            });
        }
    }
})();

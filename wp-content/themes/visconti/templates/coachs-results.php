<?php
global $wp_query;
$total  = $wp_query->found_posts;
$totalI = 0;

if(isset($_GET['title'])){
	if(have_posts()) : while(have_posts()) : the_post();
		$tempTitle = strtolower($_GET['title']);
		if(!empty($tempTitle) && strpos(strtolower(get_the_title()), strtolower($tempTitle)) !== false):
			$totalI++;
		endif;
	endwhile; endif;
}
?>
<div class="section coachs-list">
    <?php
    if (isset($_GET['title'])) {
	?>
	<div class="coach count">
		<p>
			<span class="number"><?php echo $totalI; ?></span>
			<br />
			<?php echo __('coachs dirigeants', 'vlang'); ?>
		</p>
	</div>
	<?php
        // Title Results //
        if(have_posts()) : while(have_posts()) : the_post();
		if(strpos(strtolower(get_the_title()), strtolower($_GET['title'])) !== false):
			$id     = get_the_ID();
			$termsL = get_the_terms($id, 'coach_lang');
			$termsC = get_the_terms($id, 'coach_city');
			$image  = get_field('coach_listing_image');
			$image  = wp_get_attachment_image_src( $image, 'page-coach');
			$image  = $image[0];
			$perma  = get_the_permalink();
	        ?>
	        <div class="coach">
	            <div class="picture">
	                <img src="<?php echo $image; ?>" class="fluid" alt="picture">
	            </div>

	            <div class="infos">
	                <p class="name">
		                <a href="<?php echo $perma; ?>" class="link-simple">
	                        <strong><?php echo get_the_title(); ?></strong>
		                </a>
	                </p>
	                <p class="location">
						<?php
						if( have_rows('coach_flags') ):
							$k = 0;
						    while ( have_rows('coach_flags') ) : the_row();
						        $flag = get_sub_field('coach_flag');
								$flag = wp_get_attachment_image_src( $flag, 'flag');
								$flag = $flag[0];
								if($k <= 6):
							?>
							<img src="<?php echo $flag; ?>" />
							<?php
								endif;
								$k++;
						    endwhile;
							echo '<br />';
						endif;
						?>
						<?php if($termsC):
							$j = 0;
						?>
						<?php foreach($termsC as $city):
							$name = $city->name;
							if($j != 2):
								echo '<span>'.$name.'</span>';
							endif;
							$j++;
						?>
						<?php endforeach; ?>
						<?php endif; ?>
	                </p>

	                <p class="cta hide-mobile">
	                    <a href="<?php echo $perma; ?>" class="link-simple">
	                        <i class="icon-eye"></i>
	                        <?php echo __('Voir le profil', 'vlang'); ?>
	                    </a>
                        <i class="icon-arrow-right"></i>
	                </p>

	            </div>
	        </div>
	        <?php
		endif;
        endwhile; endif;
    }else{
	?>
	<div class="coach count">
		<p>
			<span class="number"><?php echo $total; ?></span>
			<br />
			<?php echo __('coachs dirigeants', 'vlang'); ?>
		</p>
	</div>
	<?php
        // Default //
        if(have_posts()) : while(have_posts()) : the_post();
		$id     = get_the_ID();
		$termsL = get_the_terms($id, 'coach_lang');
		$termsC = get_the_terms($id, 'coach_city');
		$image  = get_field('coach_listing_image');
		$image  = wp_get_attachment_image_src( $image, 'page-coach');
		$image  = $image[0];
        $perma  = get_the_permalink();
        ?>
        <div class="coach">
            <div class="picture">
                <img src="<?php echo $image; ?>" class="fluid" alt="picture">
            </div>

            <div class="infos">
                <p class="name">
	                <a href="<?php echo $perma; ?>" class="link-simple">
                        <strong><?php echo get_the_title(); ?></strong>
	                </a>
                </p>
                <p class="location">
					<?php
					if( have_rows('coach_flags') ):
						$k = 0;
						while ( have_rows('coach_flags') ) : the_row();
							$flag = get_sub_field('coach_flag');
							$flag = wp_get_attachment_image_src( $flag, 'flag');
							$flag = $flag[0];
							if($k <= 6):
						?>
						<img src="<?php echo $flag; ?>" />
						<?php
							endif;
							$k++;
						endwhile;
						echo '<br />';
					endif;
					?>
					<?php if($termsC): ?>
						<?php $j = 0; ?>
						<?php foreach($termsC as $city):
							$name = $city->name;
							if($j != 2):
								echo '<span>'.$name.'</span>';
							endif;
							$j++;
						?>
						<?php endforeach; ?>
					<?php endif; ?>
                </p>

                <p class="cta hide-mobile">
                    <a href="<?php echo $perma; ?>" class="link-simple">
                        <i class="icon-eye"></i>
                        <?php echo __('Voir le profil', 'vlang'); ?>
                    </a>
                    <i class="icon-arrow-right"></i>
                </p>

            </div>
        </div>
        <?php
        endwhile; endif;
    }
    ?>
</div>

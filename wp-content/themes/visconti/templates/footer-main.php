<?php
$facebook   = get_field( 'facebook_url', 'options' );
$twitter    = get_field( 'twitter_url', 'options' );
$linkedin   = get_field( 'linkedin_url', 'options' );
$addressUrl = get_field( 'footer_address_url', 'options' );
$address    = get_field( 'footer_address', 'options' );
$tel        = get_field( 'footer_tel', 'options' );
$tagline    = get_field( 'footer_tagline', 'options' );
?>
<footer>

	<div class="container footer-container">
		<div class="column logo">
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/content/logofooter.png" alt="">
		</div>
		<div class="column second">
			<h3 class="title">
				<strong>
					<?php echo __( 'Visconti', 'vlang' ); ?>
				</strong>
			</h3>
			<ul class="no-style">
				<?php while ( have_rows( 'list_visconti', 'options' ) ): the_row();
					$text       = get_sub_field( 'visconti_text' );
					$url        = get_sub_field( 'visconti_url' );
					$article    = get_sub_field( 'visconti_page' );
					$articleID  = $article[0];
					$articleURL = get_permalink( $articleID );
					?>
					<li>
						<a class="link" href="<?php if ( $article ): echo $articleURL;
						else: echo $url; endif; ?>"><?php echo $text; ?></a>
					</li>
				<?php endwhile; ?>
			</ul>
		</div>
		<div class="column">
			<h3 class="title">
				<strong>
					<?php echo __( 'Annexe', 'vlang' ); ?>
				</strong>
			</h3>
			<ul class="no-style">
				<?php while ( have_rows( 'list_annex', 'options' ) ): the_row();
					$text       = get_sub_field( 'annex_text' );
					$url        = get_sub_field( 'annex_url' );
					$article    = get_sub_field( 'annex_page' );
					$articleID  = $article[0];
					$articleURL = get_permalink( $articleID );
					?>
					<li>
						<a class="link" href="<?php if ( $article ): echo $articleURL;
						else: echo $url; endif; ?>"><?php echo $text; ?></a>
					</li>
				<?php endwhile; ?>
			</ul>
		</div>
		<div class="column links">
			<h3 class="title">
				<strong>
					<?php echo __( 'Nous suivre', 'vlang' ); ?>
				</strong>
			</h3>
			<ul class="no-style">
				<?php if(!empty($facebook)):?>
				<li>
					<a href="<?php echo $facebook; ?>" class="link" target="_blank">
						<i class="icon-facebook"></i>
						<?php echo __( 'Facebook', 'vlang' ); ?>
					</a>
				</li>
				<?php endif; ?>
				<?php if(!empty($twitter)):?>
				<li>
					<a href="<?php echo $twitter; ?>" class="link" target="_blank">
						<i class="icon-twitter"></i>
						<?php echo __( 'Twitter', 'vlang' ); ?>
					</a>
				</li>
				<?php endif; ?>
				<?php if(!empty($linkedin)):?>
				<li>
					<a href="<?php echo $linkedin; ?>" class="link" target="_blank">
						<i class="icon-linkedin-square"></i>
						<?php echo __( 'Linkedin', 'vlang' ); ?>
					</a>
				</li>
				<?php endif; ?>
				<li>
					<a href="<?php echo $addressUrl; ?>" class="link" target="_blank">
						<i class="icon-map-marker"></i>
						<?php echo $address; ?>
					</a>
				</li>
				<li>
                <span>
                    <?php echo $tel; ?>
                </span>
				</li>
			</ul>

		</div>
	</div>
	<p class="container text-center">
		<?php echo $tagline; ?>
	</p>
</footer>
</div><!-- .main-wrapper-->
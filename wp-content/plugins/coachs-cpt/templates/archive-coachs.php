<?php get_header(); ?>

<div id="main-content" class="main-content">
    <form action="/" method="get" class="search-taxo-form">
        <?php generate_taxo_input( 'coach_lang' ); ?>
        <?php generate_taxo_input( 'coach_needs' ); ?>
        <?php generate_taxo_input( 'coach_city' ); ?>
        <button type="submit">Submit</button>
    </form>
    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">

        <?php
        if(have_posts()) : while(have_posts()) : the_post();
        ?>
            <li id="post-<?php the_ID(); ?>">
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            </li>
        <?php
        endwhile; endif;
        ?>
</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();
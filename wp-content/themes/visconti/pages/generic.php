<?php
/**
 * Template Name: Generique
 *
 * @package WordPress
 */

get_header(); ?>

<section class="container generic">
    <div class="generic-container flex-center-v">
        <?php the_content(); ?>
    </div>
</section>

<?php get_footer(); ?>

<?php get_header(); ?>
<?php
$global_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>9));
$current_cat  = get_the_category();
$name         = $current_cat[0]->cat_name;
$current_cat  = $current_cat[0]->term_id;
?>

<section>
	<div>
		<?php
		$blogimage = get_field('image_header_blog', 'options');
		$blogimage = wp_get_attachment_image_src( $blogimage, 'page-blog-header');
		$blogimage = $blogimage[0];
		?>
        <div class="blog-header full-background" style="background-image:url('<?php echo $blogimage; ?>');">
            <div class="container">
                <div class="content">
                    <h2 class="subtitle"><?php echo __('L\'actualité Visconti', 'vlang'); ?></h2>
                    <h1 class="title"><?php echo $name; ?></h1>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="section blog-main">
            <ul class="blog-menu js-menu no-style">
                <?php
                $categories = get_categories( $global_query );
                $i=0;
                foreach ( $categories as $category ):
                    $id   = $category->term_id;
                    $url  = get_category_link($id);
                    $name = $category->name;
                    $is_current = $current_cat && $id == $current_cat;
                    $active =  $is_current ? 'active' : '';
                    $i++;
                    ?>

                    <li class="blog-list-element <?php echo $active ?>">
                        <a href="<?php echo $url; ?>" class="link-simple category">
                            <?php if($is_current){?>
                            <i class="icon-arrow-right accent-text"></i>
                            <?php }
                            echo $name; ?>
                        </a>
                        <?php if( (empty($current_cat) && $i==1) || $is_current): ?>
                            <a href="#" class="js-open link-simple no-hover toggler">
                                <i class="accent-text icon-caret-down"></i>
                            </a>
                        <?php endif; ?>
                    </li>

                    <?php
                endforeach;
                ?>
            </ul>
            <div class="blog-list">
                <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
					<?php
					$image = get_field('blog_image');
					$image = wp_get_attachment_image_src( $image, 'page-blog');
					$image = $image[0];
                    $large_class = !empty(get_field('display_large')) ? 'big' : '';
					?>
                <div class="blog-article <?php echo $large_class; ?>">
                    <a class="picture" href="<?php the_permalink(); ?>">
                        <img src="<?php echo $image; ?>" alt="">
                    </a>

                    <div class="infos">
                        <p class="date"><?php the_date('d/m/Y'); ?></p>
                        <h3 class="title">
                            <a class="link-simple" href="<?php the_permalink(); ?>">
                                <?php the_title(); ?>
                            </a>
                        </h3>
                        <div class="content">
                            <?php the_excerpt(); ?>
                        </div>

                        <div class="accent-text">
                            <a href="<?php the_permalink(); ?>" class="article-link link-simple accent-text"><strong><?php echo __('En savoir plus', 'vlang'); ?></strong></a>
                            <i class="icon-arrow-right"></i>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>
                <?php else : ?>
                <p><?php echo __( 'Pas de résultat', 'vlang' ); ?></p>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
                <?php custom_pagination(); ?>
            </div>
        </div>

        <?php get_template_part( 'templates/home', 'insights' ); ?>
        <?php get_template_part( 'templates/general', 'twitter' ); ?>
    </div>
</section>

<?php
get_footer();

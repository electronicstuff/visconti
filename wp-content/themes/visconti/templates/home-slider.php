<?php if ( have_rows( 'home_slider' ) ): ?>
	<div class="section">
		<div class="swiper-container home-slider">
			<ul class="swiper-wrapper no-style">
				<?php while ( have_rows( 'home_slider' ) ): the_row();
					$image      = get_sub_field( 'home_slide_image' );
					$image      = wp_get_attachment_image_src( $image, 'homepage-slider' );
					$image      = $image[0];
					$title      = get_sub_field( 'home_slide_title' );
					$subtitle   = get_sub_field( 'home_slide_subtitle' );
					$text       = get_sub_field( 'home_slide_text' );
					$buttonText = get_sub_field( 'home_button_text' );
					$article    = ( get_sub_field_object( 'home_slide_article' ) );
					$articleID  = $article['value']->ID;
					$articleURL = get_permalink($article['value']);
					$articleCat = get_the_category( $articleID );
					$articleCat = $articleCat[0]->name;
					?>
					<li data-cat="<?php if ( $article ): echo $articleCat;
					else: echo __( 'Suivant', 'vlang' ); endif; ?>" class="slide swiper-slide"
					    style="background-image:url('<?php echo $image; ?>')">
						<div class="slide-container">
							<div class="content-container">
								<?php if ( $subtitle ): ?>
									<h3 class="subtitle">
										<?php echo $subtitle; ?>
									</h3>
								<?php endif; ?>
								<?php if ( $title ): ?>
									<h2 class="title">
										<?php echo $title; ?>
									</h2>
								<?php endif; ?>
								<div class="content">
									<?php if ( $text ): ?>
										<?php echo $text; ?>
									<?php endif; ?>
									<?php if ( $article ): ?>
										<a href="<?php echo $articleURL; ?>" class="link post-link">
											<?php echo $buttonText; ?>
										</a>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</li>
				<?php endwhile; ?>
			</ul>
			<div class="swiper-pagination"></div>
			<div class="swiper-button swiper-button-prev">
				<i class="icon-arrow-left"></i> <a class="link-simple" href="#"><span></span></a>
			</div>
			<div class="swiper-button swiper-button-next">
				<a class="link-simple " href="#"><span></span></a> <i class="icon-arrow-right"></i>
			</div>
		</div>
	</div>
<?php endif; ?>

<?php get_header();
global $wp_query;
$results = $wp_query->found_posts;
?>

<section class="container search-results">
    <div class="coachs-list">
    	<div class="coach count">
    		<p>
    			<span class="number"><?php echo $results; ?></span>
    			<br />
    			<?php echo __('Résultats', 'vlang'); ?>
    		</p>
    	</div>
        <?php if (have_posts()) : ?>
            <?php while(have_posts()): the_post(); ?>
                <div class="coach">
                    <div class="infos">
                        <p class="name">
                            <strong><?php the_title(); ?></strong>
                        </p>

                        <p class="cta">
                            <a href="<?php the_permalink(); ?>" class="link-simple">
                                <?php echo __('Voir la page', 'vlang'); ?>
                                <i class="icon-arrow-right"></i>
                            </a>
                        </p>

                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</section>

<?php
get_footer();

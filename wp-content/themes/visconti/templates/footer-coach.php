<?php
// @TODO: Debug this
$title   = get_field('coach_cta_title', 'options');
$page    = (get_field_object('coach_cta_url', 'options'));
$pageUrl = get_permalink($page['value'][0]);
?>

<?php if($title): ?>
<div class="cta-coach" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/content/becomecoach.png')">
    <div class="container mobile-full">
        <h4 class="text">
            <?php echo $title; ?>
        </h4><!--
        --><p class="link-container cta">
            <a href="<?php echo $pageUrl; ?>" class="link-simple icon-long-arrow-right"></a>
        </p>
    </div>
</div>
<?php endif; ?>
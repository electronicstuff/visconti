<?php
/**
 * Template Name: Page Template
 *
 * @package WordPress
 */

get_header(); ?>

<?php get_template_part( 'templates/single', 'header' ); ?>
<section>
    <div class="generic-page">
    <?php
    if ( post_password_required() ){
        echo get_the_password_form();
    } else {
        if ( have_rows( 'page_template' ) ):
            while ( have_rows( 'page_template' ) ) : the_row();
                if ( get_row_layout() == '2_col_row' ):
                    $leftTitle = get_sub_field( '2col_left_title' );
                    $leftText = get_sub_field( '2col_left_text' );
                    $rightTitle = get_sub_field( '2col_right_title' );
                    $rightText = get_sub_field( '2col_right_text' );
                    ?>
                    <div class="container">
                        <div class="section col-2">
                            <div class="col infos">
                                <h2 class="title zone-title"><?php echo $leftTitle; ?></h2>
                                <div class="content">
                                    <?php echo $leftText; ?>
                                </div>
                            </div>
                            <div class="col emphasis">
                                <h2 class="title"><?php echo $rightTitle; ?></h2>
                                <div class="content">
                                    <?php echo $rightText; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                elseif ( get_row_layout() == 'image_text' ):
                    $image = get_sub_field( 'image_text_image' );
                    $image = wp_get_attachment_image_src( $image, 'page-image-text-big' );
                    $image = $image[0];
                    $text = get_sub_field( 'page_header_text' );
                    $text = get_sub_field( 'image_text_text' );
                    ?>
                    <div class="container">
                        <div class="section page-img-text">
                            <div class="img-container">
                                <img src="<?php echo $image; ?>" alt="">
                            </div>
                            <div class="infos">
                                <?php echo $text; ?>
                            </div>
                        </div>
                    </div>
                    <?php
                elseif ( get_row_layout() == 'video_row' ):
                    $title = get_sub_field( 'video_title' );
                    $text = get_sub_field( 'video_text' );
                    $videoUrl = get_sub_field( 'video_video' );
                    $videoID = substr( $videoUrl, strrpos( $videoUrl, '?v=' ) + 3 );
                    $video = 'https://www.youtube.com/embed/' . $videoID;
                    $ctaTitle = get_sub_field( 'video_cta_title' );
                    $ctaUrl = get_sub_field( 'video_cta_url' );
                    ?>
                    <div class="container">
                        <div class="section page-video-text">

                            <?php /*
                    <p class="subtext">
                        <span>■</span>
                        <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium ea fugit quo.</span>
                    </p>
                    */ ?>

                            <div class="infos">
                                <h3 class="title">
                                    <?php echo $title; ?>
                                </h3>
                                <div class="content">
                                    <?php echo $text; ?>
                                </div>
                            </div>
                            <div class="media embed-container">
                                <iframe src="<?php echo $video; ?>" frameborder="0"
                                        allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                    <?php
                elseif ( get_row_layout() == '3_pushs_row' ):
                    if ( have_rows( 'page_pushs' ) ): ?>
                        <div class="container">
                            <div class="section col-3">
                                <div class="js-push-slider swiper-container col3-slider">
                                    <div class="js-push-wrapper">
                                        <?php
                                        while ( have_rows( 'page_pushs' ) ) : the_row();
                                            $title   = get_sub_field( 'page_push_title' );
                                            $content = get_sub_field( 'page_push_content' );
                                            ?>
                                            <div class="col js-push-item">
                                                <div class="title-container">
                                                    <h2 class="title"><?php echo $title; ?></h2>
                                                </div>
                                                <div class="content text-container">
                                                    <?php echo $content; ?>
                                                </div>
                                            </div>
                                        <?php endwhile; ?>
                                    </div>
                                    <div class="swiper-pagination positionnement-pagination"></div>
                                </div>
                            </div>
                        </div>
                        <?php
                    endif;
                elseif ( get_row_layout() == 'cta_row' ):
                    $image = get_sub_field( 'cta_image' );
                    $image = wp_get_attachment_image_src( $image, 'page-cta' );
                    $image = $image[0];
                    $text = get_sub_field( 'cta_text' );
                    $url = get_sub_field( 'cta_page' );
                    $url = get_permalink( $url[0] );
                    ?>
                    <div class="section cta-coach" style="background-image: url('<?php echo $image; ?>')">
                        <div class="container mobile-full">
                            <h4 class="text">
                                <?php echo $text; ?>
                            </h4><!--
                    --><p class="link-container cta">
                                <a href="<?php echo $url; ?>" class="link-simple icon-long-arrow-right"></a>
                            </p>
                        </div>
                    </div>
                    <?php
                elseif ( get_row_layout() == 'text_simple' ):
                    $text = get_sub_field( 'text_simple_text' );
                    ?>
                    <div class="container">
                        <div class="text-wrapper">
                            <div class="text-container text-full">
                                <?php echo $text; ?>
                            </div>
                        </div>
                    </div>
                    <?php
                elseif ( get_row_layout() == 'text_image_left' ):
                    $image = get_sub_field( 'text_image_left_image' );
                    $image = wp_get_attachment_image_src( $image, 'page-image-text' );
                    $image = $image[0];
                    $content = get_sub_field( 'text_image_left_text' );
                    ?>
                    <div class="container">
                        <div class="text-img-left text-container">
                            <img src="<?php echo $image; ?>" alt="" class="fluid">
                            <div class="content">
                                <?php echo $content; ?>
                            </div>
                        </div>
                    </div>
                    <?php
                elseif ( get_row_layout() == 'text_image_right' ):
                    $image = get_sub_field( 'text_image_right_image' );
                    $image = wp_get_attachment_image_src( $image, 'page-image-text' );
                    $image = $image[0];
                    $content = get_sub_field( 'text_image_right_text' );
                    ?>
                    <div class="container">
                        <div class="text-img-right text-container">
                            <img src="<?php echo $image; ?>" alt="" class="fluid">
                            <div class="content">
                                <?php echo $content; ?>
                            </div>
                        </div>
                    </div>
                    <?php
                elseif ( get_row_layout() == 'image_full' ):
                    $image = get_sub_field( 'image_full_image' );
                    $image = wp_get_attachment_image_src( $image, 'page-full' );
                    $image = $image[0];
                    $alt = get_sub_field( 'image_full_alt' );
                    ?>
                    <div class="container">
                        <div class="section page-img">
                            <img src="<?php echo $image; ?>" alt="<?php echo $alt; ?>">

                            <small class="copyright"><?php echo $alt; ?></small>
                        </div>
                    </div>
                    <?php
                elseif ( get_row_layout() == 'bullet_list' ):
                    $title = get_sub_field( 'bullet_title' );
                    ?>
                    <div class="container">
                        <div class="section page-list">
                            <h2 class="title zone-title hyphenate">
                                <?php echo $title; ?>
                            </h2>
                            <?php
                            if ( have_rows( 'bullet_list_left' ) ): ?>
                                <ul class="text-container list no-style">
                                    <?php
                                    while ( have_rows( 'bullet_list_left' ) ) : the_row();
                                        $content = get_sub_field( 'bullet_list_left_content' );
                                        ?>
                                        <li>
                                            <?php echo $content; ?>
                                        </li>
                                    <?php endwhile; ?>
                                </ul>
                            <?php endif; ?>
                            <?php
                            if ( have_rows( 'bullet_list_right' ) ): ?>
                                <ul class="text-container list no-style">
                                    <?php
                                    while ( have_rows( 'bullet_list_right' ) ) : the_row();
                                        $content = get_sub_field( 'bullet_list_right_content' );
                                        ?>
                                        <li>
                                            <?php echo $content; ?>
                                        </li>
                                    <?php endwhile; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php
                elseif ( get_row_layout() == 'pushs_square' ):
                    $title = get_sub_field( 'pushs_title' );
                    ?>
                    <div class="container">
                        <div class="section page-positionnement generic-push">

                            <h2 class="zone-title text-center">
                                <?php echo $title; ?>
                            </h2>

                            <div class="positionnement-slider generic-push-slider swiper-container js-push-slider">
                                <div class="positionnement-wrapper generic-push-wrapper js-push-wrapper">
                                    <?php
                                    if ( have_rows( 'push_square' ) ):
                                        while ( have_rows( 'push_square' ) ) : the_row();
                                            $image = get_sub_field( 'push_square_image' );
                                            $image = wp_get_attachment_image_src( $image, 'homepage-offre' );
                                            $image = $image[0];
                                            $text  = get_sub_field( 'push_square_text' );
                                            $url   = get_sub_field( 'push_square_url' );
                                            $url   = get_permalink( $url[0] );
                                            ?>
                                            <div class="js-push-item generic-push-item gallery-mode">
                                                <div class="generic-push-item-container">
                                                    <div class="left">
                                                        <img src="<?php echo $image; ?>" alt="">
                                                    </div>
                                                    <div class="right">
                                                        <div class="content dot-ellipsis dot-resize-update">
                                                            <strong>
                                                                <?php echo $text; ?>
                                                            </strong>
                                                        </div>
                                                        <a class="button-next corner" href="<?php echo $url; ?>">
                                                            <span class="link">→</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                        endwhile;
                                    endif;
                                    ?>
                                </div>
                                <div class="swiper-pagination positionnement-pagination"></div>
                            </div>
                        </div>
                    </div>
                    <?php
                elseif ( get_row_layout() == 'quote_row' ):
                    $title = get_sub_field( 'quote_text' );
                    $author = get_sub_field( 'quote_author' );
                    ?>
                    <div class="container">
                        <div class="section page-quote">
                            <div class="quote-wrapper">
                                <div class="bg-quotes">
                                    <p class="quote close-quotes">
                                        <?php echo $title; ?>
                                    </p>
                                </div>
                                <p class="author">
                                    <?php echo $author; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php
                elseif ( get_row_layout() == 'coach_pushs' ):
                    $title = get_sub_field( 'coach_pushs_title' );
                    ?>
                    <div class="container">
                        <div class="section page-comite">
                            <h2 class="title zone-title text-center"><?php echo $title; ?></h2>
                            <div class="js-push-slider comite-slider swiper-container">
                                <div class="js-push-wrapper coachs-list slider">
                                    <?php
                                    if ( have_rows( 'coach_repetear' ) ):
                                        while ( have_rows( 'coach_repetear' ) ) : the_row();
                                            $coach  = get_sub_field( 'coach_pushs_coach' );
                                            $title  = $coach[0]->post_title;
                                            $link   = get_permalink( $coach[0] );
                                            $id     = $coach[0]->ID;
                                            $termsL = get_the_terms( $id, 'coach_lang' );
                                            $termsC = get_the_terms( $id, 'coach_city' );
                                            $image  = get_field( 'coach_listing_image', $id );
                                            $image  = wp_get_attachment_image_src( $image, 'page-coach' );
                                            $image  = $image[0];

                                            if ( $termsL ):
                                                $lang   = $termsL[0]->name;
                                                $langid = $termsL[0]->slug;

                                                switch ( $langid ) {
                                                    case 'english':
                                                        $flag = get_template_directory_uri() . '/assets/img/flags/en.png';
                                                        break;
                                                    case 'french':
                                                        $flag = get_template_directory_uri() . '/assets/img/flags/fr.png';
                                                        break;
                                                    case 'german':
                                                        $flag = get_template_directory_uri() . '/assets/img/flags/ger.png';
                                                        break;
                                                }
                                            endif;

                                            if ( $termsC ):
                                                $city = $termsC[0]->name;
                                            endif;
                                            ?>
                                            <div class="js-push-item coach">
                                                <div class="picture">
                                                    <img src="<?php echo $image; ?>" class="fluid" alt="picture">
                                                </div>
                                                <div class="infos">
                                                    <p class="name">
                                                        <strong><?php echo $title; ?></strong>
                                                    </p>
                                                    <p class="location">
                                                        <?php if ( $flag ): ?>
                                                            <img src="<?php echo $flag; ?>" alt="flag">
                                                        <?php endif; ?>
                                                        <?php echo $city; ?>
                                                    </p>

                                                    <p class="cta hide-mobile">
                                                        <a href="<?php echo $link; ?>" class="link-simple">
                                                            <i class="icon-eye"></i>
                                                            <?php echo __( 'Voir le profil', 'vlang' ); ?>
                                                            <i class="icon-arrow-right"></i>
                                                        </a>
                                                    </p>
                                                </div>
                                            </div>
                                            <?php
                                        endwhile;
                                    endif;
                                    ?>
                                </div>
                                <div class="swiper-pagination"></div>
                            </div>
                        </div>
                    </div>
                    <?php
                elseif ( get_row_layout() == 'push_2_images' ):
                    $imageG = get_sub_field( 'push_2_left' );
                    $imageG = wp_get_attachment_image_src( $imageG, 'push-2-images' );
                    $imageG = $imageG[0];
                    $imageD = get_sub_field( 'push_2_right' );
                    $imageD = wp_get_attachment_image_src( $imageD, 'push-2-images' );
                    $imageD = $imageD[0];
                    ?>
                    <div class="container">
                        <div class="section page-img-2">
                            <div class="img-container">
                                <img class="fluid" src="<?php echo $imageG; ?>" alt="">
                            </div>
                            <div class="img-container">
                                <img class="fluid" src="<?php echo $imageD; ?>" alt="">
                            </div>
                        </div>
                    </div>
                    <?php
                elseif ( get_row_layout() == 'cta_list' ):
                    $title = get_sub_field( 'cta_list_title' );
                    ?>
                    <div class="container">
                        <div class="section page-cta-list">
                            <h2 class="title zone-title"><?php echo $title; ?></h2>
                            <?php
                            if ( have_rows( 'cta_list_inner' ) ):
                                while ( have_rows( 'cta_list_inner' ) ) : the_row();
                                    $text = get_sub_field( 'cta_list_text' );
                                    $url  = get_sub_field( 'cta_list_url' );
                                    $url  = get_permalink( $url[0] );
                                    ?>
                                    <div class="cta">
                                        <p class="text">
                                            <?php echo $text; ?>
                                        </p>
                                        <a href="<?php echo $url; ?>"
                                           class="link-simple no-hover icon-arrow-right accent"></a>
                                    </div>
                                    <?php
                                endwhile;
                            endif;
                            ?>
                        </div>
                    </div>
                    <?php
                elseif ( get_row_layout() == 'small_quote' ):
                    $quote = get_sub_field( 'small_quote_text' );
                    $author = get_sub_field( 'small_quote_author' );
                    $image = get_sub_field( 'small_quote_image' );
                    $image = wp_get_attachment_image_src( $image, 'small-quote' );
                    $image = $image[0];
                    ?>
                    <div class="container">
                        <div class="section page-quote small">
                            <div class="quote-wrapper bg-quotes">
                                <div class="picture">
                                    <img class="fluid" src="<?php echo $image; ?>" alt="">
                                </div>
                                <div class="content">
                                    <p class="quote close-quotes">
                                        <?php echo $quote; ?>
                                    </p>
                                    <p class="author">
                                        <?php echo $author; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                endif;
            endwhile;
        endif;
    }?>
    </div>
    <div class="container">
        <?php get_template_part( 'templates/home', 'insights' ); ?>
        <?php get_template_part( 'templates/general', 'twitter' ); ?>
    </div>
</section>

<?php get_footer(); ?>

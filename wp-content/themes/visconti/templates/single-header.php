<?php
$id   = get_the_id();
$page = get_page($id);
if( have_rows('page_template') ):
    while ( have_rows('page_template') ) : the_row();
        if( get_row_layout() == 'header_row' ):
            if( have_rows('page_header') ):
                while ( have_rows('page_header') ) : the_row();
                    $title = get_sub_field('page_header_title');
                    $image = get_sub_field('page_header_image');
                    $image = wp_get_attachment_image_src( $image, 'page-header');
                    $image = $image[0];
                    $text  = get_sub_field('page_header_text');
                ?>
                <div class="section">
                    <div class="page-header" style="background-image:url('<?php echo $image; ?>');">
                        <div class="container">
                            <div class="content">
                                <ul class="breadcrumbs no-style">
                                    <?php if($page->post_parent > 0):
                                        $parent     = get_page($page->post_parent);
                                        $parentName = $parent->post_title;
                                        $parentUrl  = get_permalink($parent);
                                    ?>
                                        <li class="parent crumb">
                                            <a href="<?php echo $parentUrl; ?>">
                                                <?php echo $parentName; ?>
                                            </a>
                                        </li>
                                    <?php endif; ?>
                                    <li class="crumb">
                                        <?php echo get_the_title(); ?>
                                    </li>
                                </ul>
                                <h1 class="title"><?php echo $title; ?></h1>
                                <div class="hide-mobile hide-tablet">
                                    <?php echo $text; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endwhile;
            endif;
        endif;
    endwhile;
endif;
?>

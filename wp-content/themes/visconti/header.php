<?php
	if(isset($_POST['email-input'])){
		$new_url = add_query_arg( 'success-contact', 1, get_permalink() );
		wp_redirect( $new_url, 303 );
		exit();
	}
	if (!empty($_POST['email'])){
		$new_url = add_query_arg( 'success-nl', 1, get_permalink() );
		wp_redirect( $new_url, 303 );
		exit();
	}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>
		<?php bloginfo( 'name' ); ?> |
		<?php is_front_page() ? bloginfo( 'description' ) : wp_title( '' ); ?>
	</title>

	<link rel="profile" href="http://gmpg.org/xfn/11"/>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"/>

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>


<?php if(isset($_GET['success-contact'])): ?>
<div class="message user">
	<a class="icon-cross close link-simple no-hover"></a>
	<p>
		<?php echo __('Mail envoyé.', 'vlang'); ?>
	</p>
</div>
<?php endif; ?>

<?php if (!empty($_GET['success-nl'])): ?>
	<div class="message user">
		<a class="icon-cross close link-simple no-hover"></a>
		<p>
			<?php echo __('Inscription enregistrée.', 'vlang'); ?>
		</p>
	</div>
<?php endif ?>

<?php
$title   = get_field('header_title', 'option');
$contact = get_field('page_contact', 'option');
$client  = get_field('page_client', 'option');
?>

<?php get_search_form(); ?>

<?php /* <div class="search-container">
	<div class="container">
		<form class="search">
			<input class="raw no-focus" type="text" name="query" placeholder="Votre recherche"/>

			<div>
				<a href="#" class="link-simple rounded accent">
					<?php echo __('Rechercher', 'vlang'); ?>
				</a>

				<a href="" class="js-close link-simple no-hover">
					<i class="icon-cross close"></i>
				</a>
			</div>
		</form>
	</div>
</div> */ ?>

<div class="menu-container">
	<header>
		<?php languages_list_footer(); ?>

		<?php if ( $title ): ?>
			<h1 class="main-title"><?php echo $title; ?></h1>
		<?php endif; ?>
		<p class="links">
			<?php if($contact): ?>
			<a class="link-simple" href="<?php echo $contact; ?>"><?php echo __('Contact', 'vlang'); ?></a>
			<?php endif; ?>
			<?php if($client): ?>
			<a class="link-simple" href="<?php echo $client; ?>"><?php echo __('Espace Client', 'vlang'); ?></a>
			<?php endif; ?>
			<?php /* <a class="link-simple" href="#"><?php echo __('Espace client', 'vlang'); ?></a> */?>
			<a class="link-simple hide-mobile hide-tablet js-search" href="#"><i class="icon-search"></i></a>
		</p>
	</header>
	<div class="nav-wrapper">
		<div class="container">
			<div class="nav">
				<div class="logo">
					<a href="<?php echo get_home_url(); ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/content/logo.png"
					         alt="Visconti logo">
					</a>
				</div>
				<?php if ( have_rows( 'header_links_loop', 'option' ) ): ?>
					<nav>
						<?php
	                        while ( have_rows( 'header_links_loop', 'option' ) ): the_row();
	                            $url     = get_sub_field( 'page_link_page' );
	                            $text    = get_sub_field( 'page_link_name' );
	                            $post_id = get_sub_field('page_link_page', false, false);
							?>

							<div class="link-container js-mobile-menu">
								<div class="toggler-container">
									<a class="link" href="<?php echo $url; ?>">
										<?php echo $text; ?>
									</a>
								<?php
								if(is_numeric($post_id)):
									$children = get_pages(array(
										'parent' => $post_id,
										'sort_column' => 'menu_order'
									));

									if($children):
										?>
											<a class="link toggler"></a>
										</div>
										<ul class="submenu no-style">
										<?php
										foreach($children as $child):
											$id       = $child->ID;
											$page_url = get_permalink($id);
											$title    = get_the_title($id);
										?>
										<li>
											<i class="icon-arrow-right accent-text"></i>
											<a class="link-simple" href="<?php echo $page_url; ?>"><?php echo $title; ?></a>
										</li>
										<?php
										endforeach;
										?>
										</ul>
										<?php
									else:
									?>
									</div>
									<?php
									endif;
								else:
								?>
								</div>
								<?php
								endif;
								?>
							</div>

						<?php endwhile; ?>
					</nav>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>

<div class="main-wrapper">

	<div class="mobile-menu hide-desktop">
		<a href="#" class="link-simple no-hover js-toggle-menu">
			<i class="icon-bars"></i>
			<i class="icon-cross"></i>
			<?php echo __('Visconti', 'vlang'); ?>
		</a>
		<a class="link-simple no-hover search js-search">
			<i class="icon-search"></i>
		</a>
	</div>

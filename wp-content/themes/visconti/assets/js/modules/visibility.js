(function () {

    window.visconti = window.visconti || {};
    window.visconti.visibility = visibility;

    function visibility(){
        filterBurger();
    }

    function filterBurger(){
        var burger = $('.filters-burger');
        var container = $('.filters-container');

        if(burger.length && container.length){
            burger.on('click', function(){
                container.slideToggle();
            });
        }
    }

})();
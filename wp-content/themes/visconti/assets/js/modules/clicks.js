(function () {
    var clicks;

    window.visconti = window.visconti || {};
    window.visconti.clicks = registerClicks;

    clicks = (function clicks() {
        var coaches = $('.coachs-list .coach');
        var ctaCoach = $('.cta-coach');

        return function (attach) {
            if (attach) {
                ctaCoach.add(coaches).on('click', function () {
                    var coach = $(this);
                    var href = coach.find('.cta a').attr('href');

                    if (href) {
                        window.location.href = href;
                    }
                });
            } else {
                ctaCoach.add(coaches).off('click');
            }
        }
    })();

    function registerClicks() {
        var mobile = "screen and (max-width:740px)";
        var desktop = "screen and (min-width:741px)";

        enquire
            .register(mobile, function () {
                clicks(true);
            })
            .register(desktop, function () {
                clicks(false);
            });
    }

})();
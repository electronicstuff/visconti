'use strict';

(function () {

    window.visconti = window.visconti || {};
    window.visconti.background = background;

    var $mainWrapper;
    var $mainContainer;

    var topInstance;
    var midInstance;

    function background() {
        $mainWrapper = $('.main-wrapper');
        $mainContainer = $('section.home-container');

        topInstance = topBackground();
        midInstance = midBackground();

        if (topInstance && midInstance) {
            topInstance();
            midInstance();
            event();
        }
    }

    function event() {
        $(window).on('resize', function () {
            topInstance();
            midInstance();
        });
    }

    function topBackground() {
        var $element;
        var $challenging = $('.section.challenging');

        if (!$challenging.length) {
            return false;
        }

        $element = $('<div>', {
            'class': 'background-deco top'
        });
        $mainWrapper.prepend($element);

        return function () {
            var mainTop = $mainWrapper.offset().top;
            var top = $challenging.offset().top;
            var height = $challenging.height();

            $element.css('height', top - mainTop + height / 2);
        };
    }

    function midBackground() {
        var $element;

        var $beginning = $('.section.experience');
        var $end = $beginning.next();

        if (!$beginning.length) {
            return false;
        }

        $element = $('<div>', {
            'class': 'background-deco mid'
        });
        $mainContainer.prepend($element);

        return function () {
            var mainTop = $mainContainer.offset().top;
            var beginningTop = $beginning.offset().top;
            var endTop = $end.offset().top;
            var beginningHeight = $beginning.height();
            var endHeight = $end.height();
            var elTop = beginningTop - mainTop + beginningHeight / 2;
            var elBottom = endTop - mainTop + endHeight / 2;

            $element.css({
                'top': elTop,
                'height': elBottom - elTop,
                'left': '-50px',
                'right': '-50px'
            });
        };
    }
})();
(function () {
    var clicks;

    window.visconti = window.visconti || {};
    window.visconti.clicks = registerClicks;

    clicks = function clicks() {
        var coaches = $('.coachs-list .coach');
        var ctaCoach = $('.cta-coach');

        return function (attach) {
            if (attach) {
                ctaCoach.add(coaches).on('click', function () {
                    var coach = $(this);
                    var href = coach.find('.cta a').attr('href');

                    if (href) {
                        window.location.href = href;
                    }
                });
            } else {
                ctaCoach.add(coaches).off('click');
            }
        };
    }();

    function registerClicks() {
        var mobile = "screen and (max-width:740px)";
        var desktop = "screen and (min-width:741px)";

        enquire.register(mobile, function () {
            clicks(true);
        }).register(desktop, function () {
            clicks(false);
        });
    }
})();
(function () {

    window.visconti = window.visconti || {};
    window.visconti.menu = menu;

    function menu() {
        genericMenu();
        mobileMenu();
        search();
    }

    function search() {
        var $search = $('.search-container');
        var $open = $('.js-search');
        var $close = $search.find('.js-close');

        if ($search.length && $open.length && $close.length) {
            $open.on('click', function () {
                $search.slideDown();
            });
            $close.on('click', function () {
                $search.slideUp();
            });
        }
    }

    function mobileMenu() {
        var $burger = $('.js-toggle-menu');
        var $togglers = $('.js-mobile-menu');
        var $body = $(document.body);

        if ($togglers.length) {
            $togglers.on('click', '.toggler', function (e) {
                var parent = $(e.delegateTarget);

                e.preventDefault();
                parent.toggleClass('open');
            });
        }
        if ($burger.length) {
            $burger.on('click', function () {
                $body.toggleClass('mobile-open');
            });
        }
    }

    function genericMenu() {
        var $menu = $('.js-menu');

        if ($menu.length) {
            $menu.on('click', '.js-open', function (e) {
                e.preventDefault();

                $menu.addClass('open');
            });
        }
    }
})();
(function () {

    window.visconti = window.visconti || {};
    window.visconti.message = message;

    function message() {
        var $message = $('.message.user');
        var $close = $message.find('.close');

        if ($message.length && $close.length) {
            $close.on('click', function () {
                $message.remove();
            });
        }
    }
})();
(function () {

    window.visconti = window.visconti || {};
    window.visconti.popin = popin;

    function popin() {
        coachPopin();
    }

    function coachPopin() {
        var $trigger = $('.coach-pres .consult');
        var $popin = $('.popin-contact');
        var $close = $popin.find('.close');

        if ($trigger.length && $popin.length && $close.length) {
            $trigger.on('click', function () {
                $popin.css('opacity', 0);
                $popin.removeClass('hidden');
                $popin.animate({ 'opacity': 1 });
            });
            $close.on('click', function () {
                $popin.animate({ 'opacity': 0 }, 500, function () {
                    $popin.addClass('hidden');
                });
            });
        }
    }
})();

(function () {

    window.visconti = window.visconti || {};
    window.visconti.search = search;

    function search() {
        var $form = $('.search-taxo-form');

        if ($form.length) {
            $form.on('submit', function () {
                var $this = $(this);
                var serialized = $this.serializeArray();
                var text = $.grep(serialized, function (element) {
                    return element.name == 'title';
                })[0];

                if (!text.value) {
                    text = null;
                }
                $.each(serialized, function (i, element) {
                    if (!element.value || text && element.name !== 'title') {
                        $this.find('[name=' + element.name + ']').removeAttr('name');
                    }
                });
            });
        }
    }
})();
(function () {

    window.visconti = window.visconti || {};
    window.visconti.slider = slider;

    var mobile = "screen and (min-width:320px) and (max-width:740px)";
    var tablet = "screen and (min-width:741px) and (max-width:980px)";
    var desktop = "screen and (min-width:981px)";
    var pushSlider;
    var genericInstances = [];

    function slider() {
        homeSlider();
        xpSlider();
        insightsSlider();
        twitterSlider();

        generateGenericSliders();

        registerEvents();
    }

    function homeSlider() {
        var $slider = $('.home-slider');
        var $sliderNav = $slider.children('.swiper-button');
        var $pagination = $slider.children('.swiper-pagination');
        var $texts = [$sliderNav.eq(0).find('span'), $sliderNav.eq(1).find('span')];

        if ($slider.length && $sliderNav.length && $pagination.length) {
            new Swiper($slider, {
                prevButton: $sliderNav.eq(0),
                nextButton: $sliderNav.eq(1),
                pagination: $pagination,
                loop: true,
                onSlideChangeStart: renameDirectionNav
            });
        }

        function renameDirectionNav(slider) {

            var prev;
            var next;
            var destination = slider.activeIndex;
            var total = slider.slides.length;

            prev = destination - 1;
            prev = prev < 0 ? total - 1 : prev;

            next = destination + 1;
            next = next > total - 1 ? 0 : next;

            var slides = slider.slides;
            var prevText = $(slides[prev]).data('cat');
            var nextText = $(slides[next]).data('cat');

            $texts[0].html(prevText);
            $texts[1].html(nextText);
        }
    }

    function insightsSlider() {
        var $wrapper = $('.insights .slider-container');
        var $slider = $('.slider-insights');
        var $sliderNav = $wrapper.find('.button a');
        var $pagination = $slider.children('.swiper-pagination');

        if ($slider.length && $sliderNav.length) {
            new Swiper($slider, {
                prevButton: $sliderNav.eq(0),
                nextButton: $sliderNav.eq(1),
                slidesPerView: 3,
                pagination: $pagination,
                spaceBetween: 40,
                breakpoints: {
                    740: {
                        spaceBetween: 0,
                        slidesPerView: 1,
                        centeredSlides: false
                    },
                    980: {
                        spaceBetween: 30,
                        slidesPerView: 2,
                        centeredSlides: true
                    }
                }
            });
        }
    }

    function xpSlider() {
        var xp = $('.experience');
        var $sliders = xp.find('.xp-slider');
        var $sliderNav = xp.find('.xp-slider-button a');

        if ($sliders.length) {
            $.each($sliders, function (i, slider) {
                var $slider = $(slider);

                new Swiper($slider, {
                    prevButton: $sliderNav.eq(0),
                    nextButton: $sliderNav.eq(1)
                });
            });
        }
    }

    function twitterSlider() {
        var $slider = $('.twitter-slider');

        if ($slider.length) {
            new Swiper($slider, {
                slidesPerView: 5,
                spaceBetween: 5,
                breakpoints: {
                    740: {
                        slidesPerView: 1
                    },
                    980: {
                        slidesPerView: 3
                    }
                }
            });
        }
    }

    function generateGenericSliders() {
        var sliders = ['.offers-slider', '.positionnement-slider', '.col3-slider', '.comite-slider'];

        $.each(sliders, function (i, slider) {
            var $slider = $(slider);

            if ($slider.length) {
                $slider.each(function (i, slider) {
                    genericInstances.push(genericSlider($(slider)));
                });
            }
        });
    }
    function runGenericSliders(options) {
        $.each(genericInstances, function (i, instance) {
            instance(options);
        });
    }

    function genericSlider($parent) {
        var instance;
        var $slider = $parent;
        var $wrapper = $slider.children('.js-push-wrapper');
        var $pagination = $slider.children('.swiper-pagination');
        var $slides = $slider.find('.js-push-item');

        return function (options) {
            var destroy;

            if (options) {
                destroy = options.destroy;
            }

            if (destroy) {
                if (instance) {
                    instance.destroy(true, true);
                    instance = undefined;
                }
                $slides.removeClass('swiper-slide');
                $wrapper.removeClass('swiper-wrapper');
            } else if (!destroy && !instance) {
                if ($slider.length && $pagination.length) {
                    $slides.addClass('swiper-slide');
                    $wrapper.addClass('swiper-wrapper');
                    instance = new Swiper($slider, {
                        pagination: $pagination,
                        breakpoints: {
                            740: {
                                spaceBetween: 30,
                                slidesPerView: 1,
                                centeredSlides: false
                            },
                            980: {
                                spaceBetween: 30,
                                slidesPerView: 2,
                                centeredSlides: true
                            }
                        }
                    });
                }
            }
        };
    }

    pushSlider = function () {
        var pushSwiper;
        var $slider = $('.push-slider');
        var $pagination = $slider.children('.push-pagination');

        return function (options) {
            var destroy;

            if (options) {
                destroy = options.destroy;
            }

            if (destroy && pushSwiper) {
                pushSwiper.destroy(true, true);
                pushSwiper = undefined;
            } else if (!destroy && !pushSwiper) {
                if ($slider.length && $pagination.length) {
                    pushSwiper = new Swiper($slider, {
                        pagination: $pagination,
                        breakpoints: {
                            740: {
                                spaceBetween: 30,
                                slidesPerView: 1,
                                centeredSlides: false,
                                onTap: function onTap(slider) {
                                    var $current = $(slider.slides[slider.activeIndex]);
                                    var href;

                                    if ($current.parents('.pushes').length) {
                                        href = $current.children('a').first().attr('href');
                                        if (href) {
                                            window.location.href = href;
                                        }
                                    }
                                }
                            },
                            980: {
                                spaceBetween: 30,
                                slidesPerView: 2,
                                centeredSlides: true
                            }
                        }
                    });
                }
            }
        };
    }();

    function registerEvents() {
        enquire.register(mobile, function () {
            runGenericSliders();
            pushSlider();
        }).register(tablet, function () {
            runGenericSliders();
            pushSlider();
        }).register(desktop, function () {
            runGenericSliders({
                destroy: true
            });
            pushSlider({
                destroy: true
            });
        });
    }
})();
(function () {

    window.visconti = window.visconti || {};
    window.visconti.visibility = visibility;

    function visibility() {
        filterBurger();
    }

    function filterBurger() {
        var burger = $('.filters-burger');
        var container = $('.filters-container');

        if (burger.length && container.length) {
            burger.on('click', function () {
                container.slideToggle();
            });
        }
    }
})();
$(function () {

    window.visconti.slider();
    window.visconti.visibility();
    window.visconti.popin();
    window.visconti.menu();
    window.visconti.background();
    window.visconti.clicks();
    window.visconti.search();
    window.visconti.message();
});

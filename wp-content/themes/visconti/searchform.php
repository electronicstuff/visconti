<div class="search-container">
	<div class="container mobile-full">
		<form role="search" method="get" id="searchform"
            class="search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
			<input class="raw no-focus" type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="<?php echo __('Votre recherche', 'vlang'); ?>"/>

			<div class="actions">
				<button type="submit" class="link-simple rounded accent">
					<?php echo __('Rechercher', 'vlang'); ?>
				</button>

				<a class="js-close link-simple no-hover">
					<i class="icon-cross close"></i>
				</a>
			</div>
		</form>
	</div>
</div>

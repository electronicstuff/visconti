<?php
/**
 * Post Date Time Change
 * 
 * @package    Post Date Time Change
 * @subpackage PostDateTimeChangeRegist registered in the database
    Copyright (c) 2014- Katsushi Kawamori (email : dodesyoswift312@gmail.com)
    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; version 2 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

class PostDateTimeChangeRegist {

	/* ==================================================
	 * Settings register
	 * @since	1.0
	 */
	function register_settings(){

		$user = wp_get_current_user();
		$userid = $user->ID;
		$wp_options_name = 'postdatetimechange_settings'.'_'.$userid;

		if ( !get_option($wp_options_name) ) {
			if ( !get_option('postdatetimechange_mgsettings') ) {
				$pagemax = 20;
			} else {  // old settings
				$postdatetimechange_mgsettings = get_option('postdatetimechange_mgsettings');
				$pagemax = $postdatetimechange_mgsettings['pagemax'];
				delete_option( 'postdatetimechange_mgsettings' );
			}
			$settings_tbl = array(
								'pagemax' => $pagemax
							);
			update_option( $wp_options_name, $settings_tbl );
		}

	}

}

?>
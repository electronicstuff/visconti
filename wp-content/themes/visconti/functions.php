<?php
define( 'NAKED_VERSION', 1.0 );

add_action( 'wp_enqueue_scripts', 'naked_scripts' );
function naked_scripts()  {
	wp_enqueue_style('style.css', get_stylesheet_directory_uri() . '/css/main.css');
	wp_enqueue_style('vendor.css', get_stylesheet_directory_uri() . '/css/vendor.css');

	// add theme scripts
	wp_enqueue_script( 'vendor', get_template_directory_uri() . '/js/vendor.js', array(), NAKED_VERSION, true );
	wp_enqueue_script( 'user', get_template_directory_uri() . '/js/user.js', array(), NAKED_VERSION, true );

}

// Image Sizes //
add_image_size( 'homepage-slider', 1280, 400, true);
add_image_size( 'homepage-offre', 300, 300, true);
add_image_size( 'homepage-pushs', 400, 400, true);
add_image_size( 'coach-person', 620, 350, true);
add_image_size( 'coach-track', 160, 20, true);
add_image_size( 'page-header', 1280, 400, true);
add_image_size( 'page-cta', 1024, 100, true);
add_image_size( 'page-coach', 160, 160, true);
add_image_size( 'page-image-text-big', 950, 450);
add_image_size( 'page-image-text', 620, 350);
add_image_size( 'page-full', 1024, 768, true);
add_image_size( 'page-blog', 290, 180, true);
add_image_size( 'page-blog-header', 1280, 400, true);
add_image_size( 'flag', 18, 12, true);
add_image_size( 'push-2-images', 600, 600, true);
add_image_size( 'small-quote', 50, 50, true);

add_filter('show_admin_bar', '__return_false');

// Custom Import Template //
function import_template($template_names, $load = false, $require_once = true ) {
    $located = '';
    foreach ( (array) $template_names as $template_name ) {
        if ( !$template_name )
            continue;
        if ( file_exists(STYLESHEETPATH . '/' . $template_name)) {
            $located = STYLESHEETPATH . '/' . $template_name;
            break;
        } else if ( file_exists(TEMPLATEPATH . '/' . $template_name) ) {
            $located = TEMPLATEPATH . '/' . $template_name;
            break;
        }
    }

    if ( $load && '' != $located )
        load_template( $located, $require_once );

    return $located;
}

// Custom flush on changing theme //
add_action( 'after_switch_theme', 'bt_flush_rewrite_rules' );

function bt_flush_rewrite_rules() {
     flush_rewrite_rules();
}

// ACF Options page //
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

// Custom Flags //
function languages_list_footer(){
    $languages = icl_get_languages('skip_missing=0&orderby=code');
    if(!empty($languages)){
        echo '<p class="flags">';
        foreach($languages as $l){
            if($l['country_flag_url']){
                if(!$l['active']) echo '<a href="'.$l['url'].'">';
                echo '<img src="'.$l['country_flag_url'].'" height="12" alt="'.$l['language_code'].'" width="18" />';
                if(!$l['active']) echo '</a>';
            }
        }
        echo '</p>';
    }
}

// Excerpts //
function new_excerpt_more($more) {
    return '';
}
add_filter('excerpt_more', 'new_excerpt_more');

function new_excerpt_length($length) {
	return 20;
}
add_filter( 'excerpt_length', 'new_excerpt_length', 999 );

// Custom pagionation //
function custom_pagination() {

	if( is_singular() )
		return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<ul class="no-style blog-pagination text-center">' . "\n";

	/**	Previous Post Link */
	if ( $paged != 1 )
		printf( '<li class="inactive"><a class="link-simple no-hover" href="'.get_previous_posts_page_link().'"><i class="icon-arrow-left accent-text"></i></a></li>');

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : ' class="inactive"';

		printf( '<li%s><a class="link-simple" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>…</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : ' class="inactive"';
		printf( '<li%s><a class="link-simple" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>…</li>' . "\n";
		$class = $paged == $link ? ' class="inactive"' : ' class="active"';
		printf( '<li%s><a class="link-simple" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	if ( $paged != $max )
		printf('<li class="arrow active"><a class="link-simple no-hover" href="'.get_next_posts_page_link().'"><i class="icon-arrow-right accent-text"></i></a></li>');

	echo '</ul>' . "\n";
}

// Search //
function change_wp_search_size($query) {
    if ( $query->is_search )
        $query->query_vars['posts_per_page'] = -1;

    return $query;
}
add_filter('pre_get_posts', 'change_wp_search_size');

// Unlimited coachs on coach archive //
function get_all_coach_posts( $query ) {
    if( !is_admin() && $query->is_main_query() && is_post_type_archive( 'coach' ) ) {
        $query->set( 'posts_per_page', '-1' );
    }
}
add_action( 'pre_get_posts', 'get_all_coach_posts' );

function my_password_form() {
	global $post;
	$label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
	$o = '<div class="container password-protection"><form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post">
    ' . __( "Cet article est protégé par un mot de passe. Pour le lire, veuillez saisir votre mot de passe ci-dessous:", "vlang" ) . '
    <div class="separate"><label for="' . $label . '">' . __( "Mot de passe:", "vlang" ) . ' </label><input name="post_password" id="' . $label . '" type="password" size="20" maxlength="20" /><input type="submit" name="Submit" value="' . esc_attr__( "Valider", "vlang" ) . '" /></div>
    </form></div>
    ';
	return $o;
}
add_filter( 'the_password_form', 'my_password_form' );